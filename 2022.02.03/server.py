import socket,os

server = socket.socket()
server.setsockopt(socket.SOL_SOCKET,socket.SO_REUSEADDR,1)
server.bind(("0.0.0.0",2500))
server.listen()

while True:
    klijent, adresa = server.accept()

    sta_kaze = klijent.recv(1024)

    print(sta_kaze)

    sta_kaze = sta_kaze.splitlines()
    sta_kaze = sta_kaze[0].decode().split(" ")
    trazeni_fajl = sta_kaze[1].lstrip("/")

    print(trazeni_fajl)

    document_root = "c:/Users/Grupa 1/Desktop/doc_root"

    if not trazeni_fajl:
        trazeni_fajl = 'index.html'

    if not os.path.exists(document_root+"/"+trazeni_fajl):
        klijent.send(b"HTTP/1.1 404 File Not Exist\r\n\r\n") 
        exit(0)

    sadrzaj_fajla = open(document_root+"/"+trazeni_fajl,"rb").read() 
    klijent.send(b"HTTP/1.1 200 Ok\r\nConnection:close\r\n\r\n") 
    klijent.send(sadrzaj_fajla)
    klijent.close()

