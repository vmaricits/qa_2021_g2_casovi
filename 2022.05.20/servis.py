import logging
logging.basicConfig(level=logging.DEBUG) 
from spyne import Application, rpc, ServiceBase, \
    Integer, Unicode 
from spyne import Iterable 
from spyne.protocol.soap import Soap11 
from spyne.server.wsgi import WsgiApplication 
from bank import Bank
 

application = Application([Bank],
    tns='com.service.bank',
    in_protocol=Soap11(validator='lxml'),
    out_protocol=Soap11()
)

if __name__ == '__main__': 
    from wsgiref.simple_server import make_server 
    wsgi_app = WsgiApplication(application)
    server = make_server('0.0.0.0', 8000, wsgi_app)
    server.serve_forever()