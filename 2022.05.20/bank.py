import mysql.connector as conn
import uuid  
from spyne import Application, rpc, ServiceBase, Integer, Unicode, Float, Iterable     


class Bank(ServiceBase):  
    @staticmethod
    def db(self):
        db = conn.connect(host="localhost",database="banka",user="root",passwd="")
        return db 
    session_store = {}

    @rpc(Unicode,Unicode,Integer,_returns = Integer)
    def isplata(self,token,kartica,suma):
        if token in Bank.session_store and Bank.session_store[token] == kartica:
            print("ima token")
            del Bank.session_store[token]
        else:
            return -1
        db = self.db()
        cur = db.cursor()
        cur.execute("update klijenti set stanje = stanje - %s where kartica = %s",(suma,kartica))
        cur.execute("select stanje from klijenti where kartica = %s",(kartica,))
        stanje = cur.fetchone()[0]
        db.commit()
        db.close()
        return stanje

    @rpc(Unicode,_returns = Unicode)
    def login(self, pin):
        db = self.db()
        cur = db.cursor()
        cur.execute("select * from klijenti where pin = %s",(pin,))
        if k := cur.fetchone():
            token = uuid.uuid4().hex
            kartica = k[1]
            db.close()
            Bank.session_store[token] = kartica 
            return token 
        return None

b = Bank()
t = b.login('1234')
# print(b.isplata(t,'1234123412341234',150))
