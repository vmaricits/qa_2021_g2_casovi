from flask import Flask
from flask import request
from flask import jsonify
import mysql.connector as conn

def db():
    db = conn.connect(host="localhost",database="banka",user="root",passwd="")
    return db 

app = Flask("Nas Prvi REST servis") 
@app.route("/klijent/<int:kid>",methods=["GET"])
@app.route("/klijent",methods=["GET","POST","PUT","DELETE"])
def home(kid=None):
    baza = db()
    metod = request.method.lower()
    odgovor = ""
    if metod == "get": 
        cur = baza.cursor()
        if kid:
            cur.execute("select * from klijenti where id = %s",(kid,))
            kid,kartica,stanje,pin = cur.fetchone()
            odgovor = {
                    "id":kid,
                    "kartica":kartica,
                    "stanje":stanje,
                    "pin":pin
                }
        else:
            cur.execute("select * from klijenti")
            klijenti = cur.fetchall()
            lista = []
            for kid,kartica,stanje,pin in klijenti:
                lista.append({
                    "id":kid,
                    "kartica":kartica,
                    "stanje":stanje,
                    "pin":pin
                })
            odgovor = jsonify(lista)
    elif metod == "post":
        print("metod je post")
    elif metod == "put":
        print("metod je put")
    elif metod == "delete":
        print("metod je delete")
    else:
        return odgovor 
    baza.close()
    return odgovor

app.run(debug=True)