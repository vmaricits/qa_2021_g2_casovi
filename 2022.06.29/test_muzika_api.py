import requests

def test_svi():
    svi = requests.get("http://localhost:8080/").json()
    kljucevi = ["id","naziv","najpoznatijaPesma"]
    for i in svi:
        for kljuc in kljucevi:
            assert kljuc in i

def test_jedan():
    jedan = requests.get("http://localhost:8080/1").json()
    assert jedan
    assert jedan["id"] == 1 and jedan["naziv"] == "Dovla Ricma" and jedan["najpoznatijaPesma"] == "Ej drugovi jel vam zao, rastanak se primakao"

def test_trazi():
    svi = requests.get("http://localhost:8080/trazi/drugovi").json()
    assert len(svi) == 1

# test_svi()
# test_jedan()
# test_trazi()