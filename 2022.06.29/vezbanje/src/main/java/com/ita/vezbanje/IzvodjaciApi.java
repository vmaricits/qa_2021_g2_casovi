package com.ita.vezbanje;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class IzvodjaciApi {

    @Autowired
    ServisIzvodjaci servisIzvodjaci;

    @CrossOrigin(origins = "*")
    @RequestMapping("/trazi/{kljuc}")
    public List<Izvodjac> trazi(@PathVariable("kljuc") String kljuc){
        return servisIzvodjaci.trazi(kljuc);
    }

    @CrossOrigin(origins = "*")
    @RequestMapping("/")
    public List<Izvodjac> sviIzvodjaci(){
        return servisIzvodjaci.getIzvodjaci();
    }

    @CrossOrigin(origins = "*")
    @RequestMapping("/{id}")
    public Izvodjac jedanIzvodjac(@PathVariable("id") Integer id){
        return servisIzvodjaci.getIzvodjac(id);
    }

}
