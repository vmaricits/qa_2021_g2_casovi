package com.ita.vezbanje;

import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class ServisIzvodjaci {
    public static HashMap<Integer,Izvodjac> izvodjaci = new HashMap<>(){{
            Izvodjac i1             = new Izvodjac();
            i1.id                   = 1;
            i1.naziv                = "Dovla Ricma";
            i1.najpoznatijaPesma    = "Ej drugovi jel vam zao, rastanak se primakao";
            Izvodjac i2             = new Izvodjac();
            i2.id                   = 4;
            i2.naziv                = "Ivan Katic";
            i2.najpoznatijaPesma    = "Nedostajes mi ti";
            Izvodjac i3             = new Izvodjac();
            i3.id                   = 8;
            i3.naziv                = "Dragana";
            i3.najpoznatijaPesma    = "Ja sam mala, pa sam pala, pa sam casu razlupala";
            put(1,i1);
            put(4,i2);
            put(8,i3);
    }};

    public List<Izvodjac> trazi(String kljuc){
        List<Izvodjac> res = new ArrayList<>();
        for(Map.Entry<Integer,Izvodjac> kvp : izvodjaci.entrySet()){
            if(kvp.getValue().najpoznatijaPesma.contains(kljuc)) {
                res.add(kvp.getValue());
            }
        }
        return res;
    }

    public Izvodjac getIzvodjac(int id){
        return izvodjaci.get(id);
    }
    public List<Izvodjac> getIzvodjaci(){
        List<Izvodjac> res = new ArrayList<>();
        for(Map.Entry<Integer,Izvodjac> kvp : izvodjaci.entrySet()){
            res.add(kvp.getValue());
        }
        return res;
    }

}
