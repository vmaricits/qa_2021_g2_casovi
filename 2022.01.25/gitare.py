gitare = [
    {"id":1,"naziv":"Fender Stratocaster","cena":1000,"stanje":10},
    {"id":2,"naziv":"Gibson Les Paul","cena":1200,"stanje":10},
    {"id":3,"naziv":"Ibanez Jem 7wh","cena":1500,"stanje":10},
    {"id":4,"naziv":"Gibson SG","cena":1700,"stanje":10},
    {"id":5,"naziv":"Washburn N4","cena":2500,"stanje":10},
    {"id":6,"naziv":"Jackson Adrian Smith Signature","cena":2000,"stanje":10},
    {"id":7,"naziv":"Balkanska Tambura","cena":250,"stanje":10}
]

if __name__ == "__main__":
    print(gitare)