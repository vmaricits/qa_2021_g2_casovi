from gitare import gitare as G

def get_guitar(id_gitare):
    for gitara in G:
        if gitara["id"] == id_gitare:
            return gitara
    return None

def get_guitars():
    rezultat = []
    for gitara in G:
        rezultat.append({"id":gitara["id"],"naziv":gitara["naziv"]})
    return rezultat

if __name__ == "__main__":  
    gitara = get_guitar(3)
    print(gitara)
    gitara = get_guitar(15)
    print(gitara)