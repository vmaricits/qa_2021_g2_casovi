from func import get_guitar,get_guitars

while True:
    for gitara in get_guitars():
        print(gitara["id"],gitara["naziv"])
    trazeni_id = int(input("Unesite id gitare: "))
    trazena_gitara = get_guitar(trazeni_id)
    if not trazena_gitara:
        print("Nepostojeca gitara")
        continue
    print("Odabrana gitara:")
    print(f"Naziv: {trazena_gitara['naziv']} , Cena: {trazena_gitara['cena']} , Na stanju: {trazena_gitara['stanje']}") 
    odgovor = input("Kupovina? ")
    if odgovor == 'da':
        trazena_gitara['stanje']-=1

#Test integracije
# gitara = get_guitar(3)
# if not gitara:
#     print("Gitara nije pronadjena, integracija nije dobra")
# gitara = get_guitar(4)
# if gitara:
#     print("Gitara ne bi trebalo da bude pronadjena")
# for gitara in get_guitars():
#     print(gitara["id"],gitara["naziv"])

# trazena_gitara = get_guitar(2)
# print(trazena_gitara)


