import requests
import time

broj_gresaka    = 0
broj_pokusaja   = 100
for i in range(1,broj_pokusaja+1):
    ocekivano   = i + i
    rezultat    = requests.get(f"http://jadnik.com/sab.php?a={i}&b={i}").text
    dobijeno    = int(rezultat)
    if ocekivano != dobijeno:
        broj_gresaka += 1
        print(f"greska za {i}")

print("#"*50)
print("Ukupno gresaka:",broj_gresaka)
print("Ukupno uspeha:",broj_pokusaja-broj_gresaka)