package org.example;

import static org.junit.Assert.assertTrue;

import org.junit.Assert;
import org.junit.Test;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.WebTarget;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

public class AppTest 
{
    @Test
    public void proveraRejtinga() throws IOException {
        FileReader fr = new FileReader("filmovi.txt");
        BufferedReader br = new BufferedReader(fr);
        String linija = br.readLine();
        Map<String,Integer> rejtinzi = new HashMap<>();
        rejtinzi.put("PG",0);
        rejtinzi.put("PG-13",0);
        rejtinzi.put("G",0);
        rejtinzi.put("NC-17",0);
        rejtinzi.put("R",0);
        while(linija != null){
            String[] kolone = linija.split(",");
            String kljuc = kolone[10];
            int trenutno = rejtinzi.get(kljuc);
            rejtinzi.put(kljuc,trenutno+1);
            linija = br.readLine();
        }
        Client cl = ClientBuilder.newClient();
        WebTarget wt = cl.target("http://gubitnik.com/rating.php");
        String products = wt.request().get().readEntity(String.class);
        products = products.replace("Rating,Total<hr>","");
        String[] products_arr = products.split("<br>");
        for(String s : products_arr){
            String[] kljuc_i_broj = s.split(",");
            String kljuc = kljuc_i_broj[0];
            int broj = Integer.parseInt(kljuc_i_broj[1]);
            Assert.assertEquals(broj,(int)rejtinzi.get(kljuc));
        }
    }
}
