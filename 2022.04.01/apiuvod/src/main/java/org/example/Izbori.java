package org.example;


import java.util.Random;

/**
 * Ovo je carska klasa, koju je kreirarao bata Dovla
 * i ona se koristi za predvidjanje izbornih rezultata
 *
 * @author Dovla The Greatest
 */
public class Izbori {
    /**
     * Ova metoda vraca procenat za pobedu, na osnovu imena kandidata
     * @param kandidat ime kandidata
     * @return procenat za pobedu
     */
    public int procenat(String kandidat){
        Random r = new Random();
        return r.nextInt(101);
    }
}
