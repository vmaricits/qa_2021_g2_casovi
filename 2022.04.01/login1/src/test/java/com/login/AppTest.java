package com.login;

import static org.junit.Assert.assertTrue;

import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

import javax.xml.crypto.Data;
import java.sql.Connection;
import java.sql.SQLException;

public class AppTest 
{
    @BeforeClass
    public static void setup(){
        Connection conn = Database.connect();
        try {
            for(int i=0;i<1000;i++){
                conn.createStatement().execute("insert into users values (null,'TEST_USER_"+i+"','123')");
            }

            conn.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @AfterClass
    public static void teardown(){
        Connection conn = Database.connect();
        try {
            conn.createStatement().execute("delete from users where username like 'TEST_USER_%'");
            conn.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void testJedanKorisnik(){
        User u = User.login("TEST_USER_0","123");
        Assert.assertNotNull(u);
    }

    @Test
    public void testSviKorisnici(){
        for(int i=0;i<1000;i++){
            User u = User.login("TEST_USER_"+i,"123");
            Assert.assertNotNull(u);
        }
    }


}
