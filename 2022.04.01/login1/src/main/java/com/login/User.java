package com.login;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;

public class User {
    public int id;
    public String username;
    public User(){ }
    public User(int id, String username) {
        this.id = id;
        this.username = username;
    }
    public static User login(String username, String password) {
        Connection conn = Database.connect();
        try {
            ResultSet rs = conn.createStatement().executeQuery("select * from users where username = '"+username+"' and password = '"+password+"' limit 1");
            if(rs.next()){
                return new User(rs.getInt("id"),rs.getString("username"));
            } else {
                return null;
            }
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        } finally {
            try {
                conn.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return null;
    }
}
