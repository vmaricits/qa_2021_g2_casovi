package com.login;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class Database {
    public static Connection connect(){
        Connection conn = null;
        try {
            conn = DriverManager.getConnection("jdbc:mysql://localhost/login1","root","");
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return conn;
    }
}
