import requests

polja = ["id","brand","name","price","price_sign","currency","image_link","product_link","website_link","description","rating","category","product_type","tag_list","created_at","updated_at","product_api_url","api_featured_image","product_colors"] 

def provera_polja(sminka):
    for p in polja:
        assert p in sminka 

def test_sminka():
    odgovor = requests.get("http://makeup-api.herokuapp.com/api/v1/products.json").json()
    brendovi = set({})
    for b in odgovor:
        brendovi.add(b["brand"]) 
    for b in brendovi:
        odgovor = requests.get(f"http://makeup-api.herokuapp.com/api/v1/products.json?brand={b}").json()
        for sminka in odgovor:
            provera_polja(sminka)