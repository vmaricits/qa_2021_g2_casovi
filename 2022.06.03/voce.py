import requests   

def provera_polja(voce):
    polja = ["genus", "name", "id", "family", "order", "nutritions"]
    for polje in polja:
        assert polje in voce
    nutritions = ["carbohydrates","protein","fat","calories","sugar"] 
    for polje in nutritions:
        assert polje in voce["nutritions"]  

def test_kolicina(): 
    ocekivano_komada = 31 
    odgovor = requests.get("https://fruityvice.com/api/fruit/all").json()
    assert len(odgovor) == ocekivano_komada

def test_validnost_svih_voca():
    odgovor = requests.get("https://fruityvice.com/api/fruit/all").json()  
    for voce in odgovor:
        provera_polja(voce)

def test_porodice():
    odgovor = requests.get("https://fruityvice.com/api/fruit/all").json()
    porodice = [p["family"] for p in odgovor]
    for porodica in porodice:
        odgovor = requests.get(f"https://fruityvice.com/api/fruit/family/{porodica}").json()
        for voce in odgovor:
            provera_polja(voce)

def test_genus():
    odgovor = requests.get("https://fruityvice.com/api/fruit/all").json()
    geni = [g["genus"] for g in odgovor]
    for gen in geni:
        odgovor = requests.get(f"https://fruityvice.com/api/fruit/genus/{gen}").json()
        for voce in odgovor:
            provera_polja(voce)

def test_order():
    odgovor = requests.get("https://fruityvice.com/api/fruit/all").json()
    redovi = [o["order"] for o in odgovor]
    for red in redovi:
        odgovor = requests.get(f"https://fruityvice.com/api/fruit/order/{red}").json()
        for voce in odgovor:
            provera_polja(voce)

def test_nutritions():
    nutritions = ["carbohydrates","protein","fat","calories","sugar"]
    vrednosti = [0.1,0.5,1,3,3.5,10]
    for n in nutritions: 
        for i in vrednosti:
            odgovor = requests.get(f"https://fruityvice.com/api/fruit/{n}?min=0&max={i}").json()
            if not 'error' in odgovor:
                for voce in odgovor:
                    provera_polja(voce) 
 






