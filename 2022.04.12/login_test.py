from sre_constants import SRE_INFO_LITERAL
import requests,time

data = [
    {"naziv":"Ispravno sve","un":"ivan","pwd":"234","exp":"Hello ivan"},
    {"naziv":"Neispravno sve","un":"aaa","pwd":"bbbb","exp":"Neispravan korisnik"},
    {"naziv":"Ispravan user, neispravna sifra","un":"ivan","pwd":"123","exp":"Neispravna sifra"},
    {"naziv":"Neispravan user ispravna sifra","un":"333","pwd":"234","exp":"Neispravan korisnik"},
    {"naziv":"Prazan user","un":"","pwd":"234","exp":"Hello anonimous<hr>"}
]

def test_login():
    for slucaj in data:
        naziv       = slucaj["naziv"]
        un          = slucaj["un"]  
        sifra       = slucaj["pwd"]
        ocekivano   = slucaj["exp"]
        print("***",naziv,"***")
        odgovor = requests.post("http://127.0.0.1:8000",{"un":un,"pwd":sifra}).text
        assert odgovor == ocekivano
        print("*"*50) 