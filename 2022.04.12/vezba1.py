import http.server as server
import time
import urllib.parse as parser

korisnici = {
    "goran":"123",
    "ivan":"234",
    "marija":"ceri"
}

class Obrada(server.SimpleHTTPRequestHandler):

    def do_POST(self):
        print("POST METOD")
        self.send_response(200)
        self.send_header("content-type","text/html")
        self.end_headers() 
        telo = self.rfile.read(int(self.headers["Content-Length"])) 
        parametri = dict(parser.parse_qsl(telo.decode())) 
        print(parametri) 
        odlazni_tok = self.wfile
        if "un" in parametri and "pwd" in parametri:
            if parametri["un"] in korisnici:
                if parametri["pwd"] == korisnici[parametri["un"]]:
                    odlazni_tok.write(f"Hello {parametri['un']}".encode())
                else:
                    odlazni_tok.write(b"Neispravna sifra")
            else:
                odlazni_tok.write(b"Neispravan korisnik")
        else:
            odlazni_tok.write(b"Hello anonimous<hr>") 

    def do_GET(self):  
        odlazni_tok = self.wfile
        odlazni_tok.write(b"HTTP/1.1 301 Ok\r\n")
        odlazni_tok.write(b"Connection: close\r\n")
        odlazni_tok.write(b"Content-Type: text/html\r\n") 
        odlazni_tok.write(b"\r\n") 
        
        odlazni_tok.write(b"<form method='post' >")
        odlazni_tok.write(b"Username:<br><input type='text' name='un' /><br>Password:<br><input name='pwd' type='text' />")
        odlazni_tok.write(b"<br><input value='Login' type='submit' />")
        odlazni_tok.write(b"</form>")

server.HTTPServer(("0.0.0.0",8000),Obrada).serve_forever()

