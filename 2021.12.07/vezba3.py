import time,random
import requests

broj_testova = 10

ukupno       = 0
dobrih       = 0
losih        = 0

while broj_testova > 0:
    ts          = int(time.time())
    player      = random.randint(0,10)
    full_pos    = f"{ts},{player},{player*10}" 
    full_poss   = requests.get(f"http://jadnik.com/aserver.php?ts={ts}&pl={player}").text 
    ocekivano   = full_pos
    dobijeno    = full_poss
    if ocekivano == dobijeno:
        dobrih += 1
    else:
        losih += 1
    ukupno += 1
    broj_testova -= 1

print("Ukupno testova:",ukupno)
print("Dobrih testova:",dobrih)
print("Losih testova:",losih)