from asyncio import Handle
import http.server as server 
import time,hashlib

pravi_podaci = {
    "1":{"ime":"dovla","level":10},
    "3":{"ime":"goran","level":17},
    "8":{"ime":"ivan","level":23}
}
session_storage = {

}
users = {
    "dovla":"1",
    "ivan":"8",
    "goran":"3"
}

class Hendler(server.SimpleHTTPRequestHandler):
    def do_GET(self) -> None: 
        putanja = self.path.lstrip("/").strip()
        self.send_response(200) 
        if putanja == "favicon.ico":
            self.wfile.write(f"Nema fajla".encode())
        elif putanja == "logout":
            ime = self.headers["Cookie"]
            self.end_headers() 
            del session_storage[ime]
            self.wfile.write(f"Aj cao".encode())
        elif putanja == "profile":
            ime = self.headers["Cookie"]
            self.end_headers()
            username = session_storage[ime]["ime"]
            level = session_storage[ime]["level"]
            self.wfile.write(f"Level for {username} is {level}".encode())
        else:
            if putanja in users:
                kp = pravi_podaci[users[putanja]]
                token = hashlib.md5(str(time.time()).encode()).digest().hex()
                session_storage[token] = kp
                self.send_header("Set-Cookie",f"{token}; Max-Age=100")
                self.end_headers()
                self.wfile.write(f"Hello {putanja}".encode())
            else:
                self.wfile.write(f"Neispravan korisnik".encode())




server.HTTPServer(("localhost",8000),Hendler).serve_forever()