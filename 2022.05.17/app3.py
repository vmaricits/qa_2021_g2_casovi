gradovi = [
    {"naziv":"Beograd","temp":26,"padavine":"kisa","slika":"kisa.png"},
    {"naziv":"Nis","temp":26,"padavine":"kisa","slika":"kisa.png"},
    {"naziv":"Novi Sad","temp":28,"padavine":"nema","slika":"sunce.jpg"}
]

def prognoza(g):
    for grad in gradovi:
        if grad["naziv"] == g:
            return grad
    else:
        return None

def temperatura(g):
    grad = prognoza(g)
    if grad:
        return grad["temp"]
    else:
        raise Exception("Nema Grada")

print(temperatura("Nis123"))