from xml.etree import ElementPath
import requests 
import xml.etree.ElementTree as et

sadrzaj = requests.get("http://jadnik.com:8000").text

xml = et.fromstring(sadrzaj)

def validate_slika(dobijeno):
    assert dobijeno in ["sunce.jpg","kisa.png"]

def validate_temp(grad):
    assert grad.find("temp") != None
    dobijeno = grad.find("temp").text
    assert dobijeno.isnumeric()
    dobijeno = int(dobijeno)
    assert dobijeno > -90 and dobijeno < 60

def test_gradovi():
    for grad in xml:
        naziv = grad.find("naziv").text 
        validate_temp(grad)
        padavine = grad.find("padavine").text
        slika = grad.find("slika").text 
        validate_slika(slika)

