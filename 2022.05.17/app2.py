import http.server as server

gradovi = [
    {"naziv":"Beograd","temp":26,"padavine":"kisa","slika":"kisa.png"},
    {"naziv":"Nis","temp":26,"padavine":"kisa","slika":"kisa.png"},
    {"naziv":"Novi Sad","temp":28,"padavine":"nema","slika":"sunce.jpg"}
]

def prog():
    izlaz = "<prognoza>" 
    for grad in gradovi:
        izlaz += "<grad>"
        izlaz += f"<naziv>{grad['naziv']}</naziv>"
        izlaz += f"<temp>{grad['temp']}</temp>"
        izlaz += f"<padavine>{grad['padavine']}</padavine>"
        izlaz += f"<slika>{grad['slika']}</slika>"
        izlaz += "</grad>" 
    izlaz += "</prognoza>"
    return izlaz 

class Hendler(server.SimpleHTTPRequestHandler):
    def do_GET(self):
        self.send_response(200)
        self.send_header("Content-type","application/xml")
        self.end_headers()
        self.wfile.write(prog().encode()) 

server.HTTPServer(("localhost",8000),Hendler).serve_forever()