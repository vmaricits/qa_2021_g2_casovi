package org.example;

public class Sajt {
    String link;
    double cena;

    Sajt(String link, double cena){
        this.link = link;
        this.cena = cena;
    }

    void prikazi(){
        System.out.println(link + " " + cena);
    }

}
