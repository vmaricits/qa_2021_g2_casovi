package org.example;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URL;
import java.net.URLConnection;

public class App
{
    public static void main( String[] args ) throws IOException, MalformedURLException {
        Sajt s = new Sajt("http://jadnik.com/pedala.html",10000);
        s.prikazi();
        Sajt s1 = new Sajt("http://gubitnik.com/pojacalo.html",50000);
        s1.prikazi();
        Comparator comp = new Comparator();
        System.out.println(comp.compare(s));
        System.out.println(comp.compare(s1));
    }
}
