package org.example;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;

public class Comparator {
    boolean compare(Sajt sajt_za_poredjenje) throws IOException {
        URL url = new URL(sajt_za_poredjenje.link);
        URLConnection conn = url.openConnection();
        InputStream istr = conn.getInputStream();
        BufferedReader rdr = new BufferedReader(new InputStreamReader(istr));
        String kompletno = rdr.readLine();
        kompletno = kompletno.replace("RSD","");
        kompletno = kompletno.split("Cena: ")[1];
        double cena_sa_servera = Double.valueOf(kompletno);
        return cena_sa_servera == sajt_za_poredjenje.cena;
    }
}
