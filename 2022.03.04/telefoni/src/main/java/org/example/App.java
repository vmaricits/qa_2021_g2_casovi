package org.example;

import java.util.Scanner;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args )
    {
        Scanner scanner = new Scanner(System.in);
        TelefonServis servis = new TelefonServis();
        while(true){
            System.out.println("1. Unos, 2. Prikaz, 3. Brisanje, 4. Izlaz");
            int komanda = Integer.parseInt(scanner.nextLine());
            if(komanda==1){
                System.out.println("Id?");
                int id = Integer.parseInt(scanner.nextLine());
                System.out.println("Naziv?");
                String naziv = scanner.nextLine();
                System.out.println("Cena?");
                double cena = Double.parseDouble(scanner.nextLine());
                Telefon t = new Telefon(id,naziv,cena);
                servis.dodaj(t);
            } else
            if(komanda==2){
                for(Telefon t : servis.svi()){
                    System.out.println(t.id + " " + t.naziv + " " + t.cena);
                }
            } else
            if(komanda==3){
                System.out.println("Id?");
                int id = Integer.parseInt(scanner.nextLine());
                servis.brisi(id);
            } else
            if(komanda==4){
                System.out.println("Dovidjenja");
                System.exit(0);
            }
        }
    }
}
