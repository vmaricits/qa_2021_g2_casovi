import requests
def fields(k):
    kljucevi = tuple(k.keys())
    valid = kljucevi == ("id","firstName","lastName",'fullName','title','family','image','imageUrl')
    id_kljuc = k["id"]
    valid = valid and type(id_kljuc) == type(1)
    valid = valid and type(k["firstName"]) == type("")
    return valid 

def test_jedan_karakter():
    karakter1 = [k["id"] for k in requests.get("https://thronesapi.com/api/v2/Characters").json()]
    for kid in karakter1:
        karakter = requests.get(f"https://thronesapi.com/api/v2/Characters/{kid}").json()
        assert fields(karakter)  

def test_karakteri(): 
    odgovor = requests.get("https://thronesapi.com/api/v2/Characters").json()
    for karakter in odgovor:
            assert fields(karakter) 