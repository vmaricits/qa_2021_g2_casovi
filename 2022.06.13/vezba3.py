from cgi import test
import requests
 
def validate_res(res):
    fields = ["gameID","steamAppID","cheapest","cheapestDealID","external","internalName","thumb"]
    for polje in res:
        if polje not in fields:
            return False
    return True 

def validate_game_info(res):
    fields = ["info","cheapestPriceEver","deals"]
    for polje in res:
        if polje not in fields:
            return False 
    return True 

def test_igre(): 
    url = "https://www.cheapshark.com/api/1.0/games?title=a&limit=10&exact=0"  
    response = requests.get(url).json() 
    for igra in response:
        assert validate_res(igra)


def test_game_by_steam_app_id():
    url = "https://www.cheapshark.com/api/1.0/games?title=a&limit=10&exact=0"  
    idovi = [g["steamAppID"] for g in requests.get(url).json()] 
    for gid in idovi:
        game = requests.get(f"https://www.cheapshark.com/api/1.0/games?steamAppID={gid}&limit=10&exact=0").json()
        assert validate_res(game[0])

def test_game_by_id():
    url = "https://www.cheapshark.com/api/1.0/games?title=a&limit=10&exact=0"  
    idovi = [g["gameID"] for g in requests.get(url).json()] 
    for gid in idovi:
        game = requests.get(f"https://www.cheapshark.com/api/1.0/games?id={gid}").json()
        assert validate_game_info(game)
 
