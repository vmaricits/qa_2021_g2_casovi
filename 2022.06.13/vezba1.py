import requests
nazivi = ['Westeros','Essos','Sothoryos','Ulthos']

def test_jedan_kontinent():
    kontinenti = [k["id"] for k in requests.get("https://thronesapi.com/api/v2/Continents").json()]
    for kontinent in kontinenti:
        odgovor = requests.get(f"https://thronesapi.com/api/v2/Continents/{kontinent}").json()
        print(odgovor)

        assert "id" in odgovor and "name" in odgovor and odgovor["name"] in nazivi

def test_kontinenti():

    odgovor = requests.get("https://thronesapi.com/api/v2/Continents").json()
    print(odgovor)

    
    for kontinent in odgovor:
        assert "id" in kontinent and "name" in kontinent and kontinent["name"] in nazivi
             