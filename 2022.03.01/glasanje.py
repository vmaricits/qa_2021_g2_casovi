import http.server as server
import mysql.connector as conn



class Hendler(server.SimpleHTTPRequestHandler):
    def do_GET(self) -> None:
        db = conn.connect(host="localhost",username="root",database="evrovizija",passwd="")
        cur = db.cursor()
        if self.path.startswith("/glasaj"):
            glas = int(self.path.replace("/glasaj/","")) 
            cur.execute(f"update pesme set glasovi = glasovi + 1 where id = {glas}")
            cur.execute(f"insert into adrese values (null,'{self.client_address[0]}',{glas})")
            db.commit()
         
        self.send_response(200)
        self.send_header("Content-type","text/html")
        self.end_headers() 
        
        cur.execute("select * from pesme")
        self.wfile.write(b'<meta charset="UTF-8">')
        for pid,naziv,autor,glasovi in cur.fetchall():
            izlaz = f"<a href='/glasaj/{pid}'><div style='border:1px solid red;padding:4px;margin:4px;'>{naziv} - {autor} ({glasovi})</div></a>" 
            self.wfile.write(izlaz.encode())
        db.close()
        


srv = server.HTTPServer(("",8000),Hendler)
srv.serve_forever()






