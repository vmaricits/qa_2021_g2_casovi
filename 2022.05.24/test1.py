import requests   

def test_brisanje():
    res = requests.get("http://127.0.0.1:5000").json()
    for album in res: 
        aid = album["id"]
        detalji = requests.get(f"http://127.0.0.1:5000/{aid}").json()
        album["slika"] = detalji["slika"]
        res1 = requests.delete(f"http://127.0.0.1:5000/{aid}")
    res2 = requests.get("http://127.0.0.1:5000").json()
    assert len(res2) == 0
    requests.delete("http://127.0.0.1:5000/truncate")
    for album in res:
        requests.post("http://127.0.0.1:5000",json=album)

def test_unos():
    res = requests.get("http://127.0.0.1:5000").json()
    requests.delete("http://127.0.0.1:5000/truncate") 
    podaci = [{'godina': 1980, 'id': 1, 'naziv': 'Iron Maiden','slika':''}, {'godina': 1982, 'id': 2, 'naziv': 'The number of the beast','slika':''}, {'godina': 1984, 'id': 3, 'naziv': 'Peace of mind','slika':''}, {'godina': 1992, 'id': 4, 'naziv': 'Fear of the dark','slika':''}, {'godina': 1988, 'id': 5, 'naziv': 'Seventh son of a seventh son','slika':''}, {'godina': 1981, 'id': 6, 'naziv': 'Killers','slika':''}, {'godina': 1988, 'id': 7, 'naziv': 'Seventh son of a seventh son','slika':''}]
    for album in podaci:
        requests.post("http://127.0.0.1:5000",json=album)

    res = requests.get("http://127.0.0.1:5000").json()
    assert len(res) == 7 