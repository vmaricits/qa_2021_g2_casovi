import mysql.connector as conn
from flask import Flask,request,jsonify
import time

app = Flask("Maiden service")

kljucevi = {
    "123":10,
    "234":10
}

def db():
    return conn.connect(host="localhost",database="maiden",username="root",passwd="")

@app.route("/truncate",methods=["DELETE"])
def truncate():
    baza = db()
    cur = baza.cursor()
    cur.execute("truncate table albumi")
    baza.commit()
    baza.close()
    return {"status":0}

@app.route("/<int:aid>",methods=["DELETE"])
def brisanje(aid):
    baza = db()
    cur = baza.cursor()
    cur.execute("delete from albumi where id = %s",(aid,))
    baza.commit()
    baza.close()
    return {"status":0}

@app.route("/",methods=["POST"])
def unos():
    podaci = request.json
    baza = db()
    cur = baza.cursor()
    cur.execute("insert into albumi values (null,%s,%s,%s)",(podaci["naziv"],podaci["godina"],podaci["slika"]))
    baza.commit()
    baza.close()
    return {"status":0}


@app.route("/<int:aid>")
def jedan_album(aid):
    kljuc = request.args.get("kljuc")
    if kljuc and kljuc in kljucevi and kljucevi[kljuc] > 0:
        kljucevi[kljuc]-=1
        baza = db()
        cur = baza.cursor()
        cur.execute("select * from albumi where id = %s",(aid,))  
        aid,naziv,godina,slika = cur.fetchone()
        baza.close()
        return {"id":aid,"naziv":naziv,"godina":godina,"slika":slika}, {"Access-Control-Allow-Origin":"*"}
    else:
        return {"error":"nemas kljuc"}, {"Access-Control-Allow-Origin":"*"}

@app.route("/")
def svi_albumi():
    baza = db()
    cur = baza.cursor()
    cur.execute("select * from albumi") 
    odgovor = [] 
    for aid,naziv,godina,slika in cur.fetchall():
        album = {"id":aid,"naziv":naziv,"godina":godina}
        odgovor.append(album) 
    baza.close()  
    return jsonify(odgovor), {"Access-Control-Allow-Origin":"*"}

app.run(debug=True)