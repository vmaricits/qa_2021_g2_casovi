proizvodi = [
    {"cena"            : 150,
    "naziv"           : "crep",
    "jed_mere"        : "kvm"},

    {"naziv"          : "cement",
    "cena"           : 199,
    "jed_mere"       : "kg"},

    {"naziv"          : "armatura",
    "cena"           : 100,
    "jed_mere"       : "kvm"}
]

while True:
    print("Odaberi stavku: 1 listanje, 2 pretraga, 3 izlaz")

    odabrana_stavka = input(": ")

    listanje    = odabrana_stavka == "1"
    pretraga    = odabrana_stavka == "2"
    izlaz       = odabrana_stavka == "3"

    if izlaz:
        print("Bye bye")
        exit(0)

    #listanje svih
    if listanje:
        for proizvod in proizvodi:
            print(proizvod["naziv"],proizvod["cena"],proizvod["jed_mere"])

    #pretraga
    if pretraga:
        sta_se_trazi = input("Unesi naziv: ")
        for proizvod in proizvodi:
            naziv_proizvoda = proizvod["naziv"]
            nadjeno = naziv_proizvoda == sta_se_trazi
            if nadjeno:
                print(proizvod["naziv"],proizvod["cena"],proizvod["jed_mere"])
                break
        else:
            print("Proizvod ne postoji")



