package org.example;

import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

public class KalkulatorCena {

    public String uzmi_korisnicki_input(){
        Scanner skener = new Scanner(System.in);
        String trazena_hrana = skener.nextLine();
        return trazena_hrana;
    }

    Hrana vrati_hranu(String naziv_hrane){
        Hrana h     = new Hrana();
        h.naziv     = "Sarma";
        h.cena      = 120;
        Hrana h1    = new Hrana();
        h1.naziv    = "Tufahija";
        h1.cena     = 180;
        Hrana h2    = new Hrana();
        h2.naziv    = "Kacamak";
        h2.cena     = 150;
        Map jela    = new HashMap();
        jela.put("sarma",h);
        jela.put("tufahija",h1);
        jela.put("kacamak",h2);
        Object hrana_obj = jela.get(naziv_hrane);
        Hrana moja_hrana = (Hrana)hrana_obj;
        return moja_hrana;
    }

    void run(){
        String kljuc = uzmi_korisnicki_input();
        Hrana h = this.vrati_hranu(kljuc);
        System.out.println(h.naziv+" "+h.cena);
    }

}
