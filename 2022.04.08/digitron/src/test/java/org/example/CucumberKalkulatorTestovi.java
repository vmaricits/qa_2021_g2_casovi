package org.example;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.junit.Assert;
import org.junit.Test;

public class CucumberKalkulatorTestovi {
    Kalkulator k;
    int dobijeno;
    @Given("kreiraj kalkulator sa brojevima {int} i {int}")
    public void unosBroja(int a, int b){
        k = new Kalkulator(a,b);
    }
    @When("saberi")
    public void sab(){
        dobijeno = k.saberi();
    }
    @When("oduzmi")
    public void odu(){
        dobijeno = k.oduzmi();
    }
    @When("pomnozi")
    public void pom(){
        dobijeno = k.pomnozi();
    }
    @When("podeli")
    public void pod(){
        dobijeno = k.podeli();
    }
    @Then("rezultat je {int}")
    public void rezultat(int ocekivano){
        Assert.assertEquals(ocekivano,dobijeno);
    }

}
