package org.example;

import io.cucumber.java.en.Given;
import org.junit.Assert;
import org.junit.Test;

public class JUnitKalkulatorTest {
    @Test
    public void testSabiranje(){
       Kalkulator k     = new Kalkulator(12,13);
       int ocekivano    = 25;
       int dobijeno     = k.saberi();
       Assert.assertEquals(ocekivano,dobijeno);
    }
    @Test
    public void testOduzimanje(){
        Kalkulator k    = new Kalkulator(8,3);
        int ocekivano   = 5;
        int dobijeno    = k.oduzmi();
        Assert.assertEquals(ocekivano,dobijeno);
    }
    @Test
    public void testMnozenje(){
        Kalkulator k    = new Kalkulator(5,1);
        int ocekivano   = 5;
        int dobijeno    = k.pomnozi();
        Assert.assertEquals(ocekivano,dobijeno);
    }
    @Test
    public void testDeljenje(){
        Kalkulator k    = new Kalkulator(10,2);
        int ocekivano   = 5;
        int dobijeno    = k.podeli();
        Assert.assertEquals(ocekivano,dobijeno);
    }

    @Test(expected = Exception.class)
    public void dovla() throws Exception {
        Kalkulator k    = new Kalkulator(10,2);
        k.dovla();
    }
}
