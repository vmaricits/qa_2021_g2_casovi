Feature:
  Testiranje kalkulatora, uz guaranu

  Scenario:
    Given kreiraj kalkulator sa brojevima 10 i 20
    When saberi
    Then rezultat je 30
    Given kreiraj kalkulator sa brojevima 5 i 3
    When oduzmi
    Then rezultat je 2
    Given kreiraj kalkulator sa brojevima 15 i 3
    When podeli
    Then rezultat je 5
    Given kreiraj kalkulator sa brojevima 5 i 5
    When pomnozi
    Then rezultat je 25
    Given kreiraj kalkulator sa brojevima 5 i 0
    When podeli
    Then rezultat je 0

  Scenario Outline:
    Given kreiraj kalkulator sa brojevima <prvi> i <drugi>
    When saberi
    Then rezultat je <rezultat>
    When pomnozi
    Then rezultat je <rezultat1>

    Examples:
      | prvi | drugi | rezultat | rezultat1 |
      | 10 | 20 | 30 | 200 |
      | 15 | 16 | 31 | 240 |
      | 2 | 3 | 5 | 6 |
      | 1234 | 1235 | 2469 | 1523990 |
      | 14 | 15 | 29 | 210 |

