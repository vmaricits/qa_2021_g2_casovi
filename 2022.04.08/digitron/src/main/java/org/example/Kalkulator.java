package org.example;

public class Kalkulator {
    private int broj1;
    private int broj2;
    public Kalkulator(int broj1, int broj2){
        this.broj1 = broj1;
        this.broj2 = broj2;
    }
    public int saberi(){
        return this.broj1 + this.broj2;
    }
    public int oduzmi(){
        return this.broj1 - this.broj2;
    }
    public int pomnozi(){
        return this.broj1 * this.broj2;
    }
    public int podeli(){
        return this.broj2 == 0 ? 0 : this.broj1 / this.broj2;
    }
    public void dovla() throws Exception {
        throw new Exception();
    }
}
