package org.example;
import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class App
{
    static JLabel rezultat = new JLabel("0");
    static JTextField a = new JTextField();
    static JTextField b = new JTextField();

    public static void main( String[] args )
    {
        JFrame prozor = new JFrame();
        prozor.setSize(800,600);
        prozor.setLayout(new FlowLayout());
        JLabel naslov = new JLabel("Dovlin digitron");
        prozor.getContentPane().add(naslov);
        a.setPreferredSize(new Dimension(100,20));
        b.setPreferredSize(new Dimension(100,20));
        prozor.getContentPane().add(a);
        prozor.getContentPane().add(b);
        JButton sab = new JButton("+");
        prozor.getContentPane().add(sab);
        JButton odu = new JButton("-");
        prozor.getContentPane().add(odu);
        JButton mno = new JButton("*");
        prozor.getContentPane().add(mno);
        JButton del = new JButton("/");
        prozor.getContentPane().add(del);
        sab.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                int prvi_broj_num   = Integer.parseInt(a.getText());
                int drugi_broj_num  = Integer.parseInt(b.getText());
                Kalkulator k = new Kalkulator(prvi_broj_num,drugi_broj_num);
                rezultat.setText(String.valueOf(k.saberi()));
            }
        });
        odu.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                int prvi_broj_num   = Integer.parseInt(a.getText());
                int drugi_broj_num  = Integer.parseInt(b.getText());
                Kalkulator k = new Kalkulator(prvi_broj_num,drugi_broj_num);
                rezultat.setText(String.valueOf(k.oduzmi()));
            }
        });
        mno.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                int prvi_broj_num   = Integer.parseInt(a.getText());
                int drugi_broj_num  = Integer.parseInt(b.getText());
                Kalkulator k = new Kalkulator(prvi_broj_num,drugi_broj_num);
                rezultat.setText(String.valueOf(k.pomnozi()));
            }
        });
        del.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                int prvi_broj_num   = Integer.parseInt(a.getText());
                int drugi_broj_num  = Integer.parseInt(b.getText());
                Kalkulator k = new Kalkulator(prvi_broj_num,drugi_broj_num);
                rezultat.setText(String.valueOf(k.podeli()));
            }
        });
        rezultat.setFont(new Font("Serif", Font.PLAIN, 100));
        prozor.getContentPane().add(rezultat);
        prozor.setVisible(true);
    }
}
