package entiteti;

import jakarta.persistence.*;
import java.io.Serializable;

/**
 *
 */
@Entity
@Table(name = "automobil")
public class Automobil implements Serializable {

    private static final long serialVersionUID = 1L;


    @Id
    @Column(name = "aut_id", nullable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long autId;

    @Column(name = "aut_model")
    private String autModel;

    @Column(name = "aut_snaga")
    private Integer autSnaga;

    @Column(name = "aut_proizvodjac")
    private String autProizvodjac;

    @Column(name = "aut_cena")
    private Double autCena;

    public Automobil setAutId(Long autId) {
        this.autId = autId;
        return this;
    }

    public Long getAutId() {
        return autId;
    }

    public Automobil setAutModel(String autModel) {
        this.autModel = autModel;
        return this;
    }

    public String getAutModel() {
        return autModel;
    }

    public Automobil setAutSnaga(Integer autSnaga) {
        this.autSnaga = autSnaga;
        return this;
    }

    public Integer getAutSnaga() {
        return autSnaga;
    }

    public Automobil setAutProizvodjac(String autProizvodjac) {
        this.autProizvodjac = autProizvodjac;
        return this;
    }

    public String getAutProizvodjac() {
        return autProizvodjac;
    }

    public Automobil setAutCena(Double autCena) {
        this.autCena = autCena;
        return this;
    }

    public Double getAutCena() {
        return autCena;
    }

    @Override
    public String toString() {
        return "Automobil{" +
                "autId=" + autId + '\'' +
                "autModel=" + autModel + '\'' +
                "autSnaga=" + autSnaga + '\'' +
                "autProizvodjac=" + autProizvodjac + '\'' +
                "autCena=" + autCena + '\'' +
                '}';
    }
}
