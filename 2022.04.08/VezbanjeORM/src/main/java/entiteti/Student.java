package entiteti;

import jakarta.persistence.*;
import java.io.Serializable;
import java.util.List;

@Entity
@Table(name = "student")
public class Student implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @Column(name = "std_id", nullable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long stdId;

    @Column(name = "std_imeprezime")
    private String stdImeprezime;

    @Column(name = "std_jmbg")
    private String stdJmbg;

    @Column(name = "std_godiste")
    private Integer stdGodiste;


    @Column(name = "std_pol")
    private String stdPol;

    @Column(name = "std_index")
    private String stdIndex;

    @OneToMany(mappedBy = "student", cascade = CascadeType.ALL)
    private List<Ocene> ocene;

    @ManyToMany(targetEntity = Predmet.class)
    @JoinTable(
            name = "predstud",
            joinColumns = {@JoinColumn(name = "std_id")},
            inverseJoinColumns = {@JoinColumn(name="pre_id")}
    )
    private List<Predmet> odabraniPredmeti;


    public List<Ocene> getOcene() {
        return ocene;
    }

    public void setOcene(List<Ocene> ocene) {
        this.ocene = ocene;
    }

    public Student setStdId(Long stdId) {
        this.stdId = stdId;
        return this;
    }

    public Long getStdId() {
        return stdId;
    }

    public Student setStdImeprezime(String stdImeprezime) {
        this.stdImeprezime = stdImeprezime;
        return this;
    }

    public String getStdImeprezime() {
        return stdImeprezime;
    }

    public Student setStdJmbg(String stdJmbg) {
        this.stdJmbg = stdJmbg;
        return this;
    }

    public String getStdJmbg() {
        return stdJmbg;
    }

    public Student setStdGodiste(Integer stdGodiste) {
        this.stdGodiste = stdGodiste;
        return this;
    }

    public Integer getStdGodiste() {
        return stdGodiste;
    }

    public Student setStdPol(String stdPol) {
        this.stdPol = stdPol;
        return this;
    }

    public String getStdPol() {
        return stdPol;
    }

    public Student setStdIndex(String stdIndex) {
        this.stdIndex = stdIndex;
        return this;
    }

    public void dodajOcenu(Ocene o) {
        this.ocene.add(o);
    }

    public String getStdIndex() {
        return stdIndex;
    }

    public double getProsek() {
        if( ocene.isEmpty() )
            return 0;
        double sum = 0;
        for( Ocene ocena : ocene)
            sum += ocena.getOceOcena();
        return sum/ocene.size();
    }

    @Override
    public String toString() {
        String out = stdId + " " + stdImeprezime + " " + stdGodiste;
        for(Ocene o : ocene )
            out += "{ " + o.getOceOcena() + " }";
        return out;
    }
}
