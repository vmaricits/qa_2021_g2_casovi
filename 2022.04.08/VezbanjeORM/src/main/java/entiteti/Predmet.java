package entiteti;

import jakarta.persistence.*;
import java.io.Serializable;
import java.util.List;

@Entity
@Table(name = "predmet")
public class Predmet implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @Column(name = "pre_id", nullable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long preId;

    @Column(name = "pre_naziv")
    private String preNaziv;

    @OneToMany(mappedBy = "predmet", cascade = CascadeType.ALL)
    private List<Ocene> ocene;

    @ManyToMany(targetEntity = Student.class)
    @JoinTable(
            name = "predstud",
            joinColumns = {@JoinColumn(name = "pre_id")},
            inverseJoinColumns = {@JoinColumn(name="std_id")}
    )
    private List<Student> listaStudenata;


    public List<Ocene> getOcene() {
        return ocene;
    }

    public void printOcene() {
        for(Ocene o : ocene) {
            System.out.println(o);
        }
    }

    public void setOcene(List<Ocene> ocene) {
        this.ocene = ocene;
    }

    public Predmet setPreId(Long preId) {
        this.preId = preId;
        return this;
    }

    public Long getPreId() {
        return preId;
    }

    public Predmet setPreNaziv(String preNaziv) {
        this.preNaziv = preNaziv;
        return this;
    }

    public String getPreNaziv() {
        return preNaziv;
    }

    @Override
    public String toString() {
        return "Predmet{" +
                "preId=" + preId + '\'' +
                "preNaziv=" + preNaziv + '\'' +
                '}';
    }
}
