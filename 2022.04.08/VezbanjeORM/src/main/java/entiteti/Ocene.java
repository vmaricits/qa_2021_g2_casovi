package entiteti;

import jakarta.persistence.*;
import java.io.Serializable;

/**
 *
 */
@Entity
@Table(name = "ocene")
public class Ocene implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @Column(name = "oce_id", nullable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long oceId;

    @Column(name = "oce_ocena")
    private Integer oceOcena;

    @ManyToOne
    @JoinColumn(name="std_id", referencedColumnName = "std_id")
    private Student student;

    @ManyToOne
    @JoinColumn(name="pre_id", referencedColumnName = "pre_id")
    private Predmet predmet;

    public Predmet getPredmet() {
        return predmet;
    }

    public void setPredmet(Predmet predmet) {
        this.predmet = predmet;
    }

    public Ocene setOceId(Long oceId) {
        this.oceId = oceId;
        return this;
    }

    public Long getOceId() {
        return oceId;
    }

    public Ocene setOceOcena(Integer oceOcena) {
        this.oceOcena = oceOcena;
        return this;
    }

    public Integer getOceOcena() {
        return oceOcena;
    }

    public void setStudent(Student student) {
        this.student = student;
    }

    public Student getStudent() {
        return student;
    }

    @Override
    public String toString() {
        return "Ocene{" +
                "oceId=" + oceId + '\'' +
                "oceOcena=" + oceOcena + '\'' +
                '}';
    }
}
