package entiteti;

import jakarta.persistence.*;

import java.util.Date;

@NamedQuery(name = "search by ime", query = "SELECT o FROM Osoba o WHERE o.ime = :ime")

@Entity
@Table(name="osoba")
public class Osoba {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="oso_id")
    private Integer id;

    @Column(name = "oso_ime")
    private String ime;

    @Column(name = "oso_prezime")
    private String prezime;

    @Column(name="oso_grodjenja")
    private Integer god_rodjenja;

    @Column(name = "oso_datumunosa")
    private Date datum_unosa;

    public Osoba() {

    }

    public Osoba(String ime, String prezime, Integer god_rodjenja) {
        this.ime = ime;
        this.prezime = prezime;
        this.god_rodjenja = god_rodjenja;
        this.datum_unosa = new Date();
    }

    @Override
    public String toString() {
        return "Osoba{" +
                "id=" + id +
                ", ime='" + ime + '\'' +
                ", prezime='" + prezime + '\'' +
                ", god_rodjenja=" + god_rodjenja +
                ", datum_unosa=" + datum_unosa +
                '}';
    }

    public Integer getId() {
        return id;
    }

    public String getPrezime() {
        return prezime;
    }

    public void setPrezime(String prezime) {
        this.prezime = prezime;
    }

    public Integer getGod_rodjenja() {
        return god_rodjenja;
    }

    public void setGod_rodjenja(Integer god_rodjenja) {
        this.god_rodjenja = god_rodjenja;
    }

    public Date getDatum_unosa() {
        return datum_unosa;
    }

    public void setDatum_unosa(Date datum_unosa) {
        this.datum_unosa = datum_unosa;
    }
}


