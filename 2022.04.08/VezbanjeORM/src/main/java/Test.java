import entiteti.*;
import jakarta.persistence.EntityManager;
import jakarta.persistence.EntityManagerFactory;
import jakarta.persistence.Persistence;
import jakarta.persistence.Query;

import java.util.List;

public class Test {






    public static void main(String[] args) {
        EntityManagerFactory emf =
                Persistence.createEntityManagerFactory("db_konekcija");
        EntityManager em = emf.createEntityManager();



        // Pretraga studenta po ime, prezime i indeks
        // Ocene i prosek

        String searchStr = "Pera";
        Query q =  em.createQuery("SELECT s FROM Student s WHERE s.stdImeprezime LIKE CONCAT('%', :search, '%') OR s.stdIndex" +
                " LIKE CONCAT('%', :search, '%')");
        q.setParameter("search", searchStr);
        List<Student> rezultat1 = q.getResultList();
        rezultat1.forEach( s -> {
            System.out.println(s + " prosek: " + s.getProsek());
        });


        List<Predmet> sviPredmeti = null;
        Query q1 = em.createQuery("SELECT p FROM Predmet p WHERE p.preNaziv LIKE :naziv");
        q1.setParameter("naziv", "%" + "Java" + "%");
        sviPredmeti = q1.getResultList();
        sviPredmeti.forEach( p -> { System.out.println(p); } );

        List<Student> studentiSaOcenama = null;

        Query q2 = em.createQuery("SELECT o FROM Ocene o WHERE o.oceOcena > 5 GROUP BY o.student");
        List<Ocene> sveOcene = q2.getResultList();
        sveOcene.forEach( o -> {System.out.println(o.getStudent());});

        em.close();



    }
}
