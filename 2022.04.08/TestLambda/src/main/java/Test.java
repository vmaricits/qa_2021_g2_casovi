import javax.swing.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class Test {

    public static void pretragaPoImenu(List<Osoba> listaOsoba, String ime) {
        for( Osoba o : listaOsoba ) {
            if(o.getIme().equals(ime))
                System.out.println(o);
        }
    }

    public static void pretragaOsoba(List<Osoba> listaOsoba, OsobaPretraga kriterijum) {
//        for(Osoba o : listaOsoba)
//            if( kriterijum.provera(o) )
//                System.out.println(o);
        listaOsoba.forEach( o -> {
            if( kriterijum.provera(o) )
                System.out.println(o);
        });
    }

//    public static void pretragaOsobaStr(List<Osoba> listaOsoba, OsobaPretraga kriterijum, String str) {
//        for( Osoba o : listaOsoba ) {
//            if(kriterijum.proveraStr(o, str) )
//                System.out.println(o);
//        }
//    }



    public static void main(String[] args) {
        List<Osoba> listaOsoba = new ArrayList<>();
        // dodavanje nekih osoba....

        final String strImeMail = "";
        pretragaOsoba(listaOsoba, new OsobaPretraga() {
            @Override
            public boolean provera(Osoba o) {
                return  (o.getIme().equals(strImeMail) || o.getEmail().equals(strImeMail));
            }

//            @Override
//            public boolean proveraStr(Osoba o, String str) {
//                return (o.getIme().equals(str) || o.getEmail().equals(str));
//            }
        });

        pretragaOsoba(listaOsoba, new OsobaPretraga() {
            @Override
            public boolean provera(Osoba o) {
                return o.getIme().charAt(0) == 'A';
            }
        });

        pretragaOsoba(listaOsoba, new OsobaPretraga() {
            @Override
            public boolean provera(Osoba o) {
                return o.getDatumRodjenja().getYear() >= 2000;
            }
        });


        pretragaOsoba(listaOsoba, (Osoba o) -> {
            boolean rezultat = o.getPol() == 'm';
            return rezultat;
        } );

        pretragaOsoba(listaOsoba, (Osoba o) -> {  return  o.getPol() == 'm';} );
        pretragaOsoba(listaOsoba, (Osoba o) -> o.getPol() == 'm' );
        pretragaOsoba(listaOsoba, o -> o.getPol() == 'm');

        JButton btn = new JButton();
        btn.addActionListener( l -> {
            // neki kod
        });


        listaOsoba.forEach( o -> System.out.println(o) );
        for(Osoba o :listaOsoba)
            System.out.println(o);

        HashMap<String, Integer> map = new HashMap<>();
        map.forEach( (key, value) -> {
            System.out.println("key: " + key + " value: " + value);
        }  );



        pretragaOsoba(listaOsoba, new OsobaPretraga() {
            @Override
            public boolean provera(Osoba o) {
                 return  o.getPol() == 'm';
            }
        });





    }
}
