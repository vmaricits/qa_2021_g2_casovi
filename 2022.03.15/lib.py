import mysql.connector as conn
db = conn.connect(host="localhost",username="root",passwd="",database="kursevi") 

def get_smerovi():
    cur = db.cursor() 
    cur.execute("select * from smer order by sortiranje desc")
    podaci = cur.fetchall()
    db.commit()
    return podaci

def get_kursevi(smer):
    cur = db.cursor() 
    cur.execute(f"select * from predmet where smer = {smer} order by classes asc") 
    podaci = cur.fetchall()
    db.commit()
    return podaci
