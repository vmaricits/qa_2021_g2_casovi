import http.server as server
import lib 
class Hendler(server.SimpleHTTPRequestHandler):
    def do_GET(self) -> None:
        self.send_response(200)
        self.send_header("content-type","text/html")
        self.end_headers()

        putanja = self.path
        if putanja.startswith("/smer"):
            trazeni_smer = int(putanja.replace("/smer/","")) 
            predmeti = lib.get_kursevi(trazeni_smer)
            izlaz = '<table class="tabela" border="1"><tbody><tr><td>No.</td><td>Code</td><td>Class</td><td>No. of classes</td></tr>' 
            for pid,code,pclass,classes,smer in predmeti:
                izlaz += f"<tr><td>{pid}</td><td>{code}</td><td><a href='https://www.it-akademija.com/kurs-software-design-and-architecture'>{pclass}</a></td><td>{classes}</td></tr>" 
            izlaz += '</tbody></table>' 
            self.wfile.write(izlaz.encode())
            self.wfile.close()
            return


        #prikaz smerova
        smerovi = lib.get_smerovi()
        izlaz = ""
        for sid,naziv,sortiranje in smerovi:
            izlaz+=f"<a href='/smer/{sid}'><div style='border:1px solid red;padding:4px;margin:4px;'>{naziv}</div></a>"
        self.wfile.write(izlaz.encode()) 
        self.wfile.close()

server.HTTPServer(("0.0.0.0",8000),Hendler).serve_forever()