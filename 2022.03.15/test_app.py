import lib

def test_getsmerovi():
    ocekivano = [(7, 'Gitara',10), (1, 'Python programiranje',0), (2, 'Microsoft programiranje',0), (3, 'Web Dizajn',0), (4, 'JavaScript programiranje',0), (5, 'Menadzment projekata',0), (6, 'Kuvanje',0), (8, 'Quality Assurance',0)]
    dobijeno = lib.get_smerovi()
    assert dobijeno == ocekivano

def test_getkursevi():
    dobijeno = lib.get_kursevi(8)
    for kid,code,pclass,classes,smer in dobijeno:
        assert smer == 8
