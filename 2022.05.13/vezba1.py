from selenium.webdriver import Firefox
from selenium.webdriver.common.by import By
import time
from selenium.webdriver.common.action_chains import ActionChains

hrom = Firefox()


lista_linkova = []

hrom.get("https://www.it-akademija.com")
linkovi = hrom.find_elements(By.TAG_NAME,"a")
for link in linkovi:
    href = link.get_attribute("href")
    if href not in lista_linkova:
        lista_linkova.append(href)
hrom.quit() 

for i in range(10):
    hrom = Firefox()
    hrom.get("https://www.it-akademija.com")
    linkovi = hrom.find_elements(By.TAG_NAME,"a")  
    for link in linkovi:
        href = link.get_attribute("href")
        if href in lista_linkova:
            print("Element:",href)
            lista_linkova.remove(href)
            try:
                hrom.implicitly_wait(10) 
                link.click()
                naziv_slike = str(time.time())
                hrom.save_screenshot(f"slike/{naziv_slike}.png")
            except:
                print("Neuspesan klik:",href)
            break
    hrom.quit()

print(lista_linkova) 

time.sleep(10)

hrom.quit()
