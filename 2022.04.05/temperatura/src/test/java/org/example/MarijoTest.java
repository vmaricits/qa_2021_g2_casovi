package org.example;

import org.junit.Assert;
import org.junit.Test;

public class MarijoTest {
    @Test
    public void testSabiranje(){
        Marijo m = new Marijo();
        m.a = 8;
        m.b = 2;
        int rezultat = m.sabiranje();
        int ocekivano = 10;
        Assert.assertEquals(rezultat,ocekivano);
    }
    @Test
    public void testMinus(){
        Marijo m = new Marijo();
        m.a = 8;
        m.b = -15;
        int rezultat = m.sabiranje();
        int ocekivano = -7;
        Assert.assertEquals(rezultat,ocekivano);
    }
}
