package org.example;

import org.junit.Assert;
import org.junit.Test;

public class SavetnikTest {
    @Test
    public void testMajica(){
        String dobijeno = Savetnik.odeca(30);
        String ocekivano = "Majica";
        Assert.assertEquals(dobijeno,ocekivano);
    }
    @Test
    public void testBunda(){
        String dobijeno = Savetnik.odeca(5);
        String ocekivano = "Bunda";
        Assert.assertEquals(dobijeno,ocekivano);
    }
    @Test
    public void testJaknica(){
        String dobijeno = Savetnik.odeca(15);
        String ocekivano = "Jaknica";
        Assert.assertEquals(dobijeno,ocekivano);
    }
}
