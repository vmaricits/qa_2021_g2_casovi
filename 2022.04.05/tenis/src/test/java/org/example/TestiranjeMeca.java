package org.example;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.junit.Assert;

public class TestiranjeMeca {

    Tenis t;
    int p1;
    int p2;

    @Given("pocinje teniski mec")
    public void pocinjeMec(){
        t = new Tenis();
        System.out.println("Vaistinu poc'o mec");
    }
    @When("rezultat 30,30")
    public void postavljanjeRezultata(){
        this.p1=30;
        this.p2=30;
    }
    @Then("odgovor juice")
    public void proveraOdgovora(){
        String ocekivano = "juice";
        String dobijeno = t.status(this.p1,this.p2);
        Assert.assertEquals(ocekivano,dobijeno);
    }

    @When("rezultat 55,40")
    public void rezultat5540() {
        this.p1=55;
        this.p2=40;
    }
    @When("rezultat 40,55")
    public void rezultat4055() {
        this.p1=40;
        this.p2=55;
    }
    @Then("odgovor AD")
    public void odgovor_ad() {
        String ocekivano = "AD";
        String dobijeno = t.status(this.p1,this.p2);
        Assert.assertEquals(ocekivano,dobijeno);
    }
    @When("rezultat 15,0")
    public void rezultat1500() {
        this.p1=15;
        this.p2=0;
    }
    @Then("odgovor 15 : 0")
    public void odgovor1500() {
        String ocekivano = "15 : 0";
        String dobijeno = t.status(this.p1,this.p2);
        Assert.assertEquals(ocekivano,dobijeno);
    }
    @When("rezultat 15,30")
    public void rezultat1530() {
        this.p1=15;
        this.p2=30;
    }
    @Then("odgovor 15 : 30")
    public void odgovor1530() {
        String ocekivano = "15 : 30";
        String dobijeno = t.status(this.p1,this.p2);
        Assert.assertEquals(ocekivano,dobijeno);
    }
}
