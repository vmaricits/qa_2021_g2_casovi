class Kurs:
    naziv       = ""
    kategorija  = ""
    cena        = 0 

qa              = Kurs()
qa.cena         = 1200
qa.naziv        = "Quality Assurance"
qa.kategorija   = "IT"

kk              = Kurs()
kk.cena         = 1000
kk.naziv        = "Kukicanje i pletenje za pocetnike"
kk.kategorija   = "RucniRad"

ft              = Kurs()
ft.cena         = 500
ft.naziv        = "Fotografija"
ft.kategorija   = "Umetnost"

kursevi         = (qa,kk,ft) 

max_cena        = 600

for kurs in kursevi:
    if kurs.cena <= max_cena:
        print(kurs.naziv,kurs.cena)

