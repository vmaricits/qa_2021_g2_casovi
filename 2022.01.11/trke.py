import random
import time 

class Auto:  
    def __init__(self,naziv,brzina):
        self.naziv      = naziv
        self.brzina     = brzina

class Trka:
    def __init__(self,a1,a2,cilj):
        self.a1         = a1
        self.a2         = a2
        self.cilj       = cilj
        self.pozicija1  = 0
        self.pozicija2  = 0

    def start(self): 
        while self.pozicija1 < self.cilj and self.pozicija2 < self.cilj:
            if random.randint(0,1):
                self.pozicija1 += self.a1.brzina
            if random.randint(0,1):
                self.pozicija2 += self.a2.brzina
                print(self.a1.naziv,self.pozicija1)
                print(self.a2.naziv,self.pozicija2)
            time.sleep(1)
        print("Pobednik je",self.a2.naziv if self.pozicija1 < self.pozicija2 else self.a1.naziv)