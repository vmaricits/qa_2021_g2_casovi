import random,time
neprijatelj = 100
raketa      = 25

def calc_damage(n,r):
    return n - r

def calc_heal(n):
    return n + random.randint(1,30)

while neprijatelj>0:
    neprijatelj = calc_damage(neprijatelj,raketa) 
    print("Neprijatelj hit:",neprijatelj)
    neprijatelj = calc_heal(neprijatelj)
    print("Healing:",neprijatelj)
    time.sleep(1)