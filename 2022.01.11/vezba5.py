import random,time

class Neprijatelj:
    health  = 100
    damage  = 20

    def udarac(self,drugi):
        drugi.health -= self.damage

son_pen     = Neprijatelj()
son_pen.damage = 30
son_pen.health = 120
stjuart     = Neprijatelj()
stjuart.damage = 15
stjuart.health = 90

# 0010 0000 1010 0111 0110 0010 0001 1111 0011 1010 0000



while son_pen.health > 0 and stjuart.health > 0:
    print("Son:", son_pen.health,"Stjuart:",stjuart.health)
    if random.randint(0,2)==1:
        son_pen.udarac(stjuart)
    else:
        stjuart.udarac(son_pen)
    time.sleep(1)
print("Son:", son_pen.health,"Stjuart:",stjuart.health)


