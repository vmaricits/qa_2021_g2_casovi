import random,time
neprijatelj = 100
raketa      = 25

def calc_damage():
    global neprijatelj
    global raketa
    neprijatelj -= raketa

def calc_heal():
    global neprijatelj
    neprijatelj += random.randint(1,30)

while neprijatelj>0:
    calc_damage() 
    print("Neprijatelj hit:",neprijatelj)
    calc_heal()
    print("Healing:",neprijatelj)
    time.sleep(1)