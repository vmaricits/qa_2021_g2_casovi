from trke import Trka
from trke import Auto 

class Program:
    @staticmethod
    def main(): 
        x6 = Auto("X6",120)  
        fm = Auto("Ford Mustang",200) 
        trka = Trka(x6,fm,1200)
        trka.start()