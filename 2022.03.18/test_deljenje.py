import requests 

def test_deljenje():
    odgovor = requests.get("http://gubitnik.com/div.php?a=3&b=0").text
    assert odgovor == "Result 3 / 0 = INF"
    for a in range(0,1000,123):
        for b in range(1,1000,123):
            odgovor = requests.get(f"http://gubitnik.com/div.php?a={a}&b={b}").text
            ocekivano = round(a / b , 4)
            odgovor = odgovor.split("=")
            odgovor = round(float(odgovor[1].strip()),4)
            print(ocekivano,odgovor)
            assert ocekivano == odgovor