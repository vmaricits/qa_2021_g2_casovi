import requests 

def test_filmovi1():
    fajl = open("filmovi1.txt","r")
    linija = fajl.readline()
    brojac = 0
    while linija:
        brojac += 1  
        if (brojac%20) != 0:
            linija = fajl.readline()
            continue 
        fid , naslov = linija.strip().split(",")
        odgovor = requests.get(f"http://gubitnik.com/film.php?id={fid}").text
        naslov_server = odgovor.split("|")[1] 
        assert naslov == naslov_server
        linija = fajl.readline()
    fajl.close()