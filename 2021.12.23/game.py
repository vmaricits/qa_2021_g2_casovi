import time
import os

__player_x  = 0
__player_y  = 0
__brzina    = 2
__fps       = 0.2

def __pomeranje(broj_koraka,pravac): 
    global __player_x
    global __player_y
    while broj_koraka > 0: 
        if pravac == 1:
            __player_x -= __brzina
        elif pravac == 2:
            __player_x += __brzina
        elif pravac == 3:
            __player_y -= __brzina
        elif pravac == 4:
            __player_y += __brzina
        render()
        time.sleep(__fps)
        broj_koraka-=1 

def levo(broj_koraka):
    """
    Pomeranje igraca na levo. Igrac se pomera za onoliko koraka, koliko je prosledjeno kroz parametar. Svaki korak se izvrsava u jedinici vremena, a duzina mu je izrazena kroz brzinu

    Args:
        broj_koraka: za koliko se koraka pomera igrac

    """
    __pomeranje(broj_koraka,1)

def desno(broj_koraka):
    """
    Pomeranje igraca na desno. Igrac se pomera za onoliko koraka, koliko je prosledjeno kroz parametar. Svaki korak se izvrsava u jedinici vremena, a duzina mu je izrazena kroz brzinu

    Args:
        broj_koraka: za koliko se koraka pomera igrac

    """
    __pomeranje(broj_koraka,2)

def gore(broj_koraka):
    """
    Pomeranje igraca na gore. Igrac se pomera za onoliko koraka, koliko je prosledjeno kroz parametar. Svaki korak se izvrsava u jedinici vremena, a duzina mu je izrazena kroz brzinu

    Args:
        broj_koraka: za koliko se koraka pomera igrac

    """
    __pomeranje(broj_koraka,3)

def dole(broj_koraka):
    """
    Pomeranje igraca na dole. Igrac se pomera za onoliko koraka, koliko je prosledjeno kroz parametar. Svaki korak se izvrsava u jedinici vremena, a duzina mu je izrazena kroz brzinu

    Args:
        broj_koraka: za koliko se koraka pomera igrac

    """
    __pomeranje(broj_koraka,4)

def pozicija():
    print(__player_x,__player_y)

def render(w=30,h=20):
    """
    Iscrtavanje displeja. Brise se sadrzaj terminala, i iscrtava se displej, u rezoluciji predstavljenoj ulaznim parametrima

    Args:
        w: sirina displeja (podrazumevani parametar)
        h: visina displeja (podrazumevani parametar)
    """
    os.system("cls")
    for y in range(h):
        for x in range(w):
            sprajt = " "
            if y == 0 or y == h-1 or x == 0 or x == w-1:
                sprajt = "#"
            if __player_y == y and __player_x == x:
                sprajt = "@"
            print(sprajt,end="")
        print()


