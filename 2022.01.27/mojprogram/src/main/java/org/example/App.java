package org.example;

import java.net.*;
import java.io.*;

public class App 
{
    public static void main( String[] args ) throws IOException
    {
        ServerSocket server        = new ServerSocket(80);
        Socket klijent             = server.accept();
        InputStream ulazni_tok     = klijent.getInputStream();
        OutputStream izlazni_tok   = klijent.getOutputStream();
        InputStreamReader iis       = new InputStreamReader(ulazni_tok);
        BufferedReader br           = new BufferedReader(iis);
        while(true){
            String linija = br.readLine();
            if(linija.isEmpty()){
                break;
            }
            System.out.println(linija);
        }
        izlazni_tok.write("HTTP/1.1 404 Ok\r\n".getBytes());
        izlazni_tok.write("\r\n".getBytes());
        izlazni_tok.write("Hello From Dovla!!!".getBytes());
        klijent.close();
    }
}
