from selenium.webdriver import Chrome,ChromeOptions
from selenium.webdriver.common.by import By
import time
import threading 

opcije = ChromeOptions()
opcije.add_argument("--log-level=3")
opcije.add_argument("--no-sandbox")
#opcije.add_argument("--headless") 

def jedan_test(broj_testa):
    test_data = {
        "Metal":["Metallica","Sepultura","SMF"],
        "Hard Rock":["Iron Maiden","AC DC","Tool"],
        "Pop":["Michael Jackson","Madonna","Dzordz Majkl"]
    }
    print(f"Test: {broj_testa}....")
    hrom = Chrome(options=opcije) 
    hrom.get("http://gresnik.com/muzika.php")   
    hrom.save_screenshot("slika.png")
    for ind,zanr in enumerate(test_data.items()):
        zanr,grupe = zanr 
        time.sleep(1)
        zanr_select = hrom.find_element(By.ID,"zanr")
        zanr_select.click() 
        zanrovi = zanr_select.find_elements(By.TAG_NAME,"option")
        zanrovi[ind+1].click()
        time.sleep(0.1) 
        assert zanrovi[ind+1].text == zanr 
        time.sleep(0.1) 
        for grupa_ind,grupa in enumerate(grupe):
            grupa_select = hrom.find_element(By.ID,"grupa")
            grupa_select.click()
            grupe = grupa_select.find_elements(By.TAG_NAME,"option")
            grupe[grupa_ind+1].click()  
            izlaz = hrom.find_element(By.ID,"izlaz")
            ocekivani_izlaz = f"Odabrao si grupu {grupa}"
            dobijeni_izlaz = izlaz.text
            assert ocekivani_izlaz == dobijeni_izlaz   
    hrom.quit()
    print(f"Test {broj_testa} je kompletiran")

for i in range(1):
    threading.Thread(None,jedan_test,args=[i]).start()