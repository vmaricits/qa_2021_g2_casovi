package org.example;

import java.util.ArrayList;
import java.util.List;

public class App
{
    public static void main( String[] args )
    {
        Model m = new Model();
        UI ui = new UI();
        while(true){
            ui.prikazi_praznike(m.svi_praznici());
            Praznik p = m.praznik(ui.uzmi_input());
            p.details();
        }
    }
}
