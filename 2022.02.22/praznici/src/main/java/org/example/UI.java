package org.example;

import java.util.List;
import java.util.Scanner;

public class UI {
    /***
     * Uzima se korisnicki input
     * @return int id praznika
     */
    int uzmi_input(){
        System.out.println("Unesi id praznika:");
        Scanner sc = new Scanner(System.in);
        String unos = sc.nextLine();
        int unos_broj = Integer.parseInt(unos);
        return unos_broj;
    }
    void prikazi_praznike(List<Praznik> praznici){
        for(Praznik praznik : praznici){
            praznik.info();
        }
    }
}
