package org.example;

import java.util.ArrayList;
import java.util.List;

public class Model {
    List<Praznik> praznici;
    Model(){
        praznici = new ArrayList<>();
        Praznik nova_godina = new Praznik(1,"Nova Godina","Novogodisnje veselje. Svi pijani. Slavi se do 15 februara u Srbiji");
        Praznik bozic = new Praznik(2,"Bozic","Dzizus se rodi. Vaistinu se rodi");
        Praznik vikend = new Praznik(3,"Vikend","Nema rada, pa moze da se odradi posao koji je preostao od radne nedelje");
        praznici.add(nova_godina);
        praznici.add(bozic);
        praznici.add(vikend);
    }
    List<Praznik> svi_praznici(){
        return this.praznici;
    }
    Praznik praznik(int id){
        boolean pronadjen = false;
        for(Praznik praznik : praznici){
            if(praznik.id == id) {
                return praznik;
            }
        }
        return null;
    }
}
