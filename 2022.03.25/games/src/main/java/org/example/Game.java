package org.example;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;

public class Game {
    Connection connect(){
        try {
            return DriverManager.getConnection("jdbc:mysql://localhost/games","root","");
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }
    int maxGold(){
        try {
            ResultSet res = connect().createStatement().executeQuery("select max(gold) as gold from games");
            if(res.next()){
                int vrednost_kolone = res.getInt("gold");
                return vrednost_kolone;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return -1;
    }
    int maxKills(){
        try {
            ResultSet res = connect().createStatement().executeQuery("select max(kills) as kills from games");
            if(res.next()){
                int vrednost_kolone = res.getInt("kills");
                return vrednost_kolone;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return -1;
    }
    int maxDeaths(){
        try {
            ResultSet res = connect().createStatement().executeQuery("select max(deaths) as deaths from games");
            if(res.next()){
                int vrednost_kolone = res.getInt("deaths");
                return vrednost_kolone;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return -1;
    }
}
