package org.example;

import org.junit.*;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class GameTest {

    @BeforeClass
    public static void preKlase(){
        try {
            Connection conn = DriverManager.getConnection("jdbc:mysql://localhost/games","root","");
            conn.createStatement().execute("insert into games values (null,900,900,900)");
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @AfterClass
    public static void posleKlase(){
        try {
            Connection conn = DriverManager.getConnection("jdbc:mysql://localhost/games","root","");
            conn.createStatement().execute("delete from games where kills=900 and deaths = 900 and gold = 900");
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Before
    public void preMetode(){
        System.out.println("Ovo se izvrsi pre svake test metode");
    }

    @After
    public void posleMetode(){
        System.out.println("Ovo se izvrsi posle svake test metode");
    }

    @Test
    public void testMaxGold(){
        Game g = new Game();
        int ocekivano = 900;
        int dobijeno = g.maxGold();
        Assert.assertEquals(ocekivano,dobijeno);
    }
    @Test
    public void testMaxKills(){
        Game g = new Game();
        int ocekivano = 900;
        int dobijeno = g.maxKills();
        Assert.assertEquals(ocekivano,dobijeno);
    }
    @Test
    public void testMaxDeaths(){
        Game g = new Game();
        int ocekivano = 900;
        int dobijeno = g.maxDeaths();
        Assert.assertEquals(ocekivano,dobijeno);
    }
}
