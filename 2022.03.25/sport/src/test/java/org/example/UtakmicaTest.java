package org.example;

import org.junit.Assert;
import org.junit.Test;

public class UtakmicaTest {
    @Test
    public void vreme(){
        Utakmica1 u = new Utakmica1("Zvezda","Borac");
        String ocekivano = "Prvo poluvreme";
        u.vreme = 40;
        Assert.assertEquals(ocekivano,u.vreme());
    }
    @Test
    public void rezultat(){
        Utakmica1 u = new Utakmica1("Zvezda","Borac");
        u.rezultat_domaci = 2;
        u.rezultat_gosti = 2;
        String ocekivano = "Nereseno (2 : 2)";
        Assert.assertEquals(ocekivano,u.rezultat());
    }
    @Test
    public void prviGol(){
        Utakmica1 u = new Utakmica1("Zvezda","Borac");
        u.golovi.add("Zvezda");
        String ocekivano = "Zvezda";
        Assert.assertEquals(ocekivano,u.prviGol());
    }
}
