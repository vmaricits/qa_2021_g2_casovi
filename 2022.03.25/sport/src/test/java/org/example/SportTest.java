package org.example;

import org.junit.Assert;
import org.junit.Test;

public class SportTest {

    @Test
    public void vreme(){
        Utakmica u = new Utakmica();
        String ocekivano = "Drugo poluvreme";
        for(int i=1;i<46;i++){
            Assert.assertEquals("Prvo poluvreme",u.vreme(i));
        }
        for(int i=46;i<91;i++){
            Assert.assertEquals("Drugo poluvreme",u.vreme(i));
        }
        Assert.assertEquals("Pauza",u.vreme(-1));
    }

    @Test
    public void rezultat(){
        String ocekivano = "Nereseno (2 : 2)";
        String dobijeno = Utakmica.rezultat("Zvezda","Partizan",2,2);
        Assert.assertEquals(ocekivano,dobijeno);
    }

    @Test
    public void rezultatZvezdaVodi(){
        Utakmica u = new Utakmica();
        String ocekivano = "Vodi tim Zvezda protiv Partizan (2-1)";
        String dobijeno = u.rezultat("Zvezda","Partizan",2,1);
        Assert.assertEquals(ocekivano,dobijeno);
    }

    @Test
    public void rezultatPartizanVodi(){
        Utakmica u = new Utakmica();
        String ocekivano = "Vodi tim Partizan protiv Zvezda (3-2)";
        String dobijeno = u.rezultat("Zvezda","Partizan",2,3);
        Assert.assertEquals(ocekivano,dobijeno);
    }

    @Test
    public void prviGol(){
        Utakmica u = new Utakmica();
        String ocekivano = "Zvezda";
        String dobijeno = u.prviGol();
        Assert.assertEquals(dobijeno,ocekivano);
    }

}
