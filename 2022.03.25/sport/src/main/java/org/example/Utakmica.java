package org.example;

public class Utakmica {
    String vreme(int t){
        if(t>0 && t<46){
            return "Prvo poluvreme";
        } else if(t>45 && t<91){
            return "Drugo poluvreme";
        } else {
            return "Pauza";
        }
    }
    static String rezultat(String tim1, String tim2, int s1, int s2){
        if(s1>s2){
            return String.format("Vodi tim %s protiv %s (%s)",tim1,tim2,String.valueOf(s1)+"-"+String.valueOf(s2));
        } else if(s2>s1){
            return String.format("Vodi tim %s protiv %s (%s)",tim2,tim1,String.valueOf(s2)+"-"+String.valueOf(s1));
        } else {
            return String.format("Nereseno (%d : %d)",s1,s2);
        }
    }
    String prviGol(){
        return "Zvezda";
    }
}
