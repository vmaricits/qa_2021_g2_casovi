package org.example;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args )
    {
        new Thread(new Runnable() {
            @Override
            public void run() {
                new Utakmica1("Zvezda","Borac").start();
            }
        }).start();
        new Thread(new Runnable() {
            @Override
            public void run() {
                new Utakmica1("Chelsea","Liverpool").start();
            }
        }).start();
        new Thread(new Runnable() {
            @Override
            public void run() {
                new Utakmica1("Manchester United","Milan").start();
            }
        }).start();
    }
}
