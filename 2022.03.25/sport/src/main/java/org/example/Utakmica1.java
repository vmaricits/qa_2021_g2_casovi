package org.example;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class Utakmica1 {
    String domaci;
    String gosti;
    int rezultat_domaci;
    int rezultat_gosti;
    List<String> golovi;
    int vreme;
    Utakmica1(String domaci, String gosti){
        this.domaci     = domaci;
        this.gosti      = gosti;
        this.golovi     = new ArrayList<String>();
    }
    String vreme(){
        if(this.vreme > 0 && this.vreme < 46)
            return "Prvo poluvreme";
        else
        if(this.vreme > 45 && this.vreme < 91)
            return "Drugo poluvreme";
        else
            return "Pauza";
    }
    String rezultat(){
        return Utakmica.rezultat(this.domaci,this.gosti,rezultat_domaci,rezultat_gosti);
    }
    String prviGol(){
        return golovi.size() > 0 ? golovi.get(0) : "Niko";
    }
    void spavanje(){
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
    void start(){
        Random r = new Random();
        System.out.println("Pocetak utakmice");
        for(int i=0;i<46;i++){
            this.vreme = i;
            System.out.println(vreme());
            System.out.println(rezultat());
            spavanje();
            rezultat_domaci+=r.nextInt(5)==1?1:0;
            rezultat_gosti+=r.nextInt(5)==1?1:0;
        }
        for(int i=-15;i<0;i++){
            this.vreme = i;
            System.out.println(vreme());
            System.out.println(rezultat());
            spavanje();
        }
        for(int i=46;i<91;i++){
            this.vreme = i;
            System.out.println(vreme());
            System.out.println(rezultat());
            spavanje();
            rezultat_domaci+=r.nextInt(5)==1?1:0;
            rezultat_gosti+=r.nextInt(5)==1?1:0;
        }
        System.out.println("Kraj utakmice");
        rezultat();
    }
}
