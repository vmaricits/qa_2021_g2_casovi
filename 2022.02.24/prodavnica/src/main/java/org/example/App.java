package org.example;


import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.Socket;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args ) throws IOException, SQLException, InterruptedException {

      Connection conn = DriverManager.getConnection("jdbc:mysql://localhost/patike", "root", "");
      ResultSet rs = conn.createStatement().executeQuery("select patike.naziv as pn,marke.naziv as mn,tipovi.naziv as tn, group_concat(brojevipatike.broj) from patike join marke on patike.marka = marke.id join tipovi on patike.tip = tipovi.id join brojevipatike on brojevipatike.patika = patike.id where group by pn,mn,tn");

      while(rs.next()){
          System.out.println(rs.getString(1)+" "+rs.getString(2)+" "+rs.getString(3)+" "+rs.getString(4));
      }

    }
}
