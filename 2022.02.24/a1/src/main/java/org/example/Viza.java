package org.example;

public class Viza implements IKartica {
    int balance = 200;

    @Override
    public void setBalance(int balance) {
        this.balance = balance;
    }

    @Override
    public int getBalance() {
        return this.balance;
    }
}
