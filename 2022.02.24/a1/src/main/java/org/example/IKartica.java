package org.example;

public interface IKartica {
    void setBalance(int balance);
    int getBalance();
}
