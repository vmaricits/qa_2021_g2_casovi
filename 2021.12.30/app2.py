from http.server import SimpleHTTPRequestHandler
from http.server import HTTPServer
import urllib.parse as parse

kolaci = {
    '1':{"naziv":"Hurmasica","cena":200,"slika":"https://podravkaiovariations.azureedge.net/9c0f716a-63b4-11eb-b14c-0242ac12004b/v/f2b1f6a6-64bc-11eb-b6c2-0242ac130010/1024x768-f2b21802-64bc-11eb-a115-0242ac130010.webp"},
    '2':{"naziv":"Kadaif","cena":250,"slika":"https://podravkaiovariations.azureedge.net/566d9b32-6404-11eb-88e5-0242ac120039/v/f2b1f6a6-64bc-11eb-b6c2-0242ac130010/1024x768-f2b21802-64bc-11eb-a115-0242ac130010.webp"},
    '3':{"naziv":"Baklava","cena":220,"slika":"https://i.ytimg.com/vi/6lhASdsdpok/maxresdefault.jpg"},
    '4':{"naziv":"Krempita","cena":260,"slika":"https://podravkaiovariations.azureedge.net/496e9bfc-63b4-11eb-a06d-0242ac120060/v/f2b1f6a6-64bc-11eb-b6c2-0242ac130010/1024x768-f2b21802-64bc-11eb-a115-0242ac130010.webp"}
} 

class NasProgram(SimpleHTTPRequestHandler):
    def do_GET(self):

        params =  dict(parse.parse_qsl(parse.urlparse(self.path).query))

        if 'id' in params:
            kid = params['id']
            kolac = kolaci[kid]
            self.wfile.write(b"HTTP/1.1 200 Ok\r\nContent-type: text/html\r\n\r\n")
            self.wfile.write(f"<h2>{kolac['naziv']}</h2>".encode())
            self.wfile.write(f"<img src='{kolac['slika']}' width=200 />".encode())
            self.wfile.write(f"<br>Cena: {kolac['cena']}".encode())
            return

        izlaz = "<select onchange='window.location=\"?id=\"+this.value'>"
        for i in kolaci:
            izlaz += f"<option value='{i}'>{kolaci[i]['naziv']}</option>" 
        izlaz += "</select>"

        self.wfile.write(b"HTTP/1.1 200 Ok\r\nContent-type: text/html\r\n\r\n")
        self.wfile.write(izlaz.encode())
        


HTTPServer(("localhost",8000),NasProgram).serve_forever()