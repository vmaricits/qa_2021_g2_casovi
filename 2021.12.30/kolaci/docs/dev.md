## Developerska dokumentacija

Program je baziran na Python modulu http.server.
Korisnicka klasa je NasProgram i ona nasledjuje klasu SimpleHTTPRequestHandler.

Za parsiranje, koriscen je modul <b>urllib.parse</b>.

Primer koda 1:

    izlaz = "<select onchange='window.location=\"?id=\"+this.value'>"
    for i in kolaci:
        izlaz += f"<option value='{i}'>{kolaci[i]['naziv']}</option>" 
    izlaz += "</select>"

Primer koda 2:

    self.wfile.write(b"HTTP/1.1 200 Ok\r\nContent-type: text/html\r\n\r\n")
    self.wfile.write(f"<h2>{kolac['naziv']}</h2>".encode())
    self.wfile.write(f"<img src='{kolac['slika']}' width=200 />".encode())
    self.wfile.write(f"<br>Cena: {kolac['cena']}".encode())