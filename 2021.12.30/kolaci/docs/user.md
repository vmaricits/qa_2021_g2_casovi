## Korisnicka dokumentacija

Aplikacija se koristi tako sto se pristupi strani [http://gresnik.com](http://gresnik.com){target='_blank'}

Po otvaranju strane, korisnik ima mogucnost odabira jednog od kolaca iz padajuce liste.

![meni](meni.png)
<br>Slika: 1.

Nakon odabira kolaca, otvara se strana sa prikazom detalja o kolacu. Detalji o kolacu su naziv, cena i slika.

![baklava](baklava.png)

