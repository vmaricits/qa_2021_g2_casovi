# Najjaca aplikacija za kupovinu kolaca

#### Opis

Aplikacija omogucava da se odabere jedan iz liste kolaca. Po odabiru, prikazuju se slika, naziv i cena kolaca.

Korisnik programa, moze slobodno mastati o kolacu, ali ga ne sme jesti. Zasto? Zato sto je sve sto je lepo ZABRANJENO!

#### Tehnologije

- [Python](https://python.org){target='_blank'}
- HTTP
- HTML

#### Autori

Najistrajniji u drugoj grupi <b>QA 2021</b>

[Dovla master](dovla.md)

[Ivan Hunter](ivan.md)

[Senad, lover](senad.md)