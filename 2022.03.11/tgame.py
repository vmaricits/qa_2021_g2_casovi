from lib import is_hidden
import pytest

@pytest.fixture
def dovla():
    print("Krece testiranje")

def test_granica_u_offsetu(dovla):
    ocekivano   = False
    bb          = (10,10,5,5)
    pl          = (10,10) 
    dobijeno = is_hidden(bb,pl)
    assert ocekivano == dobijeno

def test_granica_izvan_objekta():
    ocekivano   = False
    bb          = (10,10,5,5)
    pl          = (9,9) 
    dobijeno = is_hidden(bb,pl)
    assert ocekivano == dobijeno

def test_u_okviru_objekta():
    ocekivano   = True
    bb          = (10,10,5,5)
    pl          = (13,13) 
    dobijeno = is_hidden(bb,pl)
    assert ocekivano == dobijeno