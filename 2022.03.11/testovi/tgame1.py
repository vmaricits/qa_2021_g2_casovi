import unittest
from lib import is_hidden 

class TestIgre(unittest.TestCase):
    def test_granica_u_offsetu(self):
        ocekivano   = False
        bb          = (10,10,5,5)
        pl          = (10,10) 
        dobijeno = is_hidden(bb,pl)
        self.assertEqual(ocekivano,dobijeno)

    def test_granica_izvan_objekta(self):
        ocekivano   = False
        bb          = (10,10,5,5)
        pl          = (9,9) 
        dobijeno = is_hidden(bb,pl)
        self.assertEqual(ocekivano,dobijeno)

    def test_u_okviru_objekta(self):
        ocekivano   = True
        bb          = (10,10,5,5)
        pl          = (13,13) 
        dobijeno = is_hidden(bb,pl)
        self.assertEqual(ocekivano,dobijeno) 
