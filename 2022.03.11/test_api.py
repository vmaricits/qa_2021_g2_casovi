import requests

def test_voce():
    banane = requests.get("http://gresnik.com/voce.html").text
    banane,cena = banane.strip().split(",")
    cena = int(cena)
    ocekivano_naziv = "banane"
    ocekivano_cena  = 300
    assert ocekivano_cena == cena
    assert ocekivano_naziv == banane

def test_patike():
    patike = requests.get("http://gresnik.com/patike.html").text
    patike,cena = patike.strip().split(",") 
    cena = int(cena)
    ocekivano_naziv = "najke"
    ocekivano_cena = 150
    assert ocekivano_cena == cena
    assert ocekivano_naziv == patike
