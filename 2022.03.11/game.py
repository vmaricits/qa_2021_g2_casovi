from lib import is_hidden
import time,sys,os

def render(drvo,igrac):
    for y in range(20):
        for x in range(20):
            sprajt = " "
            if y >= drvo[1] and y <= drvo[1] + drvo[3] and x >= drvo[0] and x<=drvo[0] + drvo[2]:
                sprajt = "#"
            if x == igrac[0] and y == igrac[1]:
                sprajt = "@"
            print(sprajt,end="")
        print("")


drvo = (6,7,2,3)
player = [3,5]
for i in range(10):
    os.system("cls")
    render(drvo,player)

    if is_hidden(drvo,player):
        print("Igrac je nevidljiv")
    else:
        print("Igrac je vidljiv")
    player[0]+=1
    player[1]+=1
    
    time.sleep(1)
