import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import java.io.File;
import java.util.ArrayList;

public class TaskManagement {

    private ArrayList<Task> listTasks = new ArrayList<>();

    public void readTasks() throws Exception {
        Document doc = XMLFasada.open(new File("tasks.xml"));
        NodeList tasks = doc.getElementsByTagName("task");
        for( int i = 0 ; i < tasks.getLength() ; i++ ) {
            Node task = tasks.item(i);
            if( task.getNodeType() == Node.ELEMENT_NODE ) {
                Element eTask = (Element) task;

                NodeList titles = eTask.getElementsByTagName("title");
                if( titles.getLength() == 0 ) continue;
                String title = titles.item(0).getTextContent();

                NodeList descs = eTask.getElementsByTagName("desc");
                if( descs.getLength() == 0 ) continue;
                String desc = descs.item(0).getTextContent();

                NodeList assigntos = eTask.getElementsByTagName("assignto");
                if ( assigntos.getLength() == 0 ) continue;
                String assignto = assigntos.item(0).getTextContent();

                this.listTasks.add(new Task(title.trim(), desc.trim(), assignto.trim()));
            }
        }
    }

    public void printAllTasks(String osoba) {
        for(Task t : listTasks )
            if( osoba == null )
                System.out.println(t);
            else {
                if( t.getAssignto().equals(osoba) )
                    System.out.println(t);
            }
    }

    public void addTask(Task t) throws Exception {
        Document doc = XMLFasada.open(new File("tasks.xml"));

        Element newTask = doc.createElement("task");
        Element title = doc.createElement("title");
        Element desc = doc.createElement("desc");
        Element assignto = doc.createElement("assignto");
        title.setTextContent(t.getTitle());
        desc.setTextContent(t.getDesc());
        assignto.setTextContent(t.getAssignto());
        newTask.appendChild(title);
        newTask.appendChild(desc);
        newTask.appendChild(assignto);
        doc.getDocumentElement().appendChild(newTask);

        XMLFasada.save(doc, new File("tasks.xml"));

    }


    public static void main(String[] args) throws Exception {
        TaskManagement tm = new TaskManagement();
        tm.readTasks();
        tm.printAllTasks(null);
        System.out.println("========");
        tm.printAllTasks("Pera");

        tm.addTask(new Task("test", "test", "test"));

    }
}
