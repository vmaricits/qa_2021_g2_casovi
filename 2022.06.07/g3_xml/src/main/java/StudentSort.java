import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import java.io.File;
import java.util.ArrayList;

public class StudentSort {
    public static void main(String[] args) throws  Exception{
        Document doc = XMLFasada.open(new File("studenti.xml"));
        String grad = "Beograd";
        ArrayList<Student> listaStudenata = new ArrayList<>();

        NodeList sviStudenti = doc.getElementsByTagName("student");
        for( int i = 0 ; i < sviStudenti.getLength() ; i++ ) {
            Student noviStudent = new Student();
            Element elem = (Element) sviStudenti.item(i);
            noviStudent.setGrad(elem.getElementsByTagName("grad").item(0).getTextContent());
            noviStudent.setIme(elem.getElementsByTagName("ime").item(0).getTextContent());
            noviStudent.setPrezime(elem.getElementsByTagName("prezime").item(0).getTextContent());
            noviStudent.setIndeks(elem.getElementsByTagName("indeks").item(0).getTextContent());
            if ( grad.equals(noviStudent.getGrad()))
                listaStudenata.add(noviStudent);
        }

//        listaStudenata.sort( (s1, s2) ->  s1.getIndeks().compareTo(s2.getIndeks()) );
        listaStudenata.sort ( (s1, s2) -> {
            String[] stud1 = s1.getIndeks().split("/");
            String[] stud2 = s2.getIndeks().split("/");

            if( Integer.parseInt(stud1[1]) == Integer.parseInt(stud2[1]) )
                return Integer.parseInt(stud1[0]) - Integer.parseInt(stud2[0]);
            else {
                return Integer.parseInt(stud1[1]) - Integer.parseInt(stud2[1]);
            }

        });


        for(Student s : listaStudenata )
            System.out.println(s);




    }
}
