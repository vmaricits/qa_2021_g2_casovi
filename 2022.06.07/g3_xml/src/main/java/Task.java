public class Task {

    private String title, desc;
    private String assignto;

    public Task() {}


    @Override
    public String toString() {
        return "Task{" +
                "title='" + title + '\'' +
                ", desc='" + desc + '\'' +
                ", assignto='" + assignto + '\'' +
                '}';
    }

    public Task(String title, String desc, String assignto) {
        this.title = title;
        this.desc = desc;
        this.assignto = assignto;
    }

    public String getTitle() {
        return title;
    }


    public void setTitle(String title) {
        this.title = title;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public String getAssignto() {
        return assignto;
    }

    public void setAssignto(String assignto) {
        this.assignto = assignto;
    }
}
