import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamConstants;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.Characters;
import javax.xml.stream.events.EndElement;
import javax.xml.stream.events.StartElement;
import javax.xml.stream.events.XMLEvent;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.net.URL;
import java.util.ArrayList;
import java.util.Comparator;

public class RSSFeed {
    public static void main(String[] args) {

        ArrayList<RSSitem> listItems = new ArrayList<>();
        XMLInputFactory factory = XMLInputFactory.newInstance();
        boolean isDesc = false, isTitle = false, isLink = false, isItem = false;
        RSSitem item = null;
        try {
            URL url = new URL("http://feeds.bbci.co.uk/news/science_and_environment/rss.xml");
            XMLEventReader eventReader = factory.createXMLEventReader(url.openStream());

            while(eventReader.hasNext()) {

                XMLEvent event = eventReader.nextEvent();
                switch ( event.getEventType() ) {
                    case XMLStreamConstants.START_ELEMENT:
                        StartElement openTag = event.asStartElement();
                        String tagName = openTag.getName().getLocalPart();
                        if( tagName.equalsIgnoreCase("item") ) {
                            isItem = true;
                            item = new RSSitem();
                        }
                        else if (tagName.equalsIgnoreCase("title") ) isTitle = true;
                        else if ( tagName.equalsIgnoreCase("link") ) isLink = true;
                        else if ( tagName.equalsIgnoreCase("description")) isDesc = true;

                        break;
                    case XMLStreamConstants.CHARACTERS:
                        Characters characters = event.asCharacters();

                        if( isTitle && item != null ) item.setTitle(characters.getData().trim());
                        else if( isDesc && item != null) item.setDesc(characters.getData().trim());
                        else if( isLink && item != null ) item.setLink(characters.getData().trim());

                        break;
                    case XMLStreamConstants.END_ELEMENT:
                        EndElement closedTag = event.asEndElement();
                        tagName = closedTag.getName().getLocalPart();

                        if( tagName.equalsIgnoreCase("item") ) {
                            isItem = false;
                            listItems.add(item);
                            item = null;
                        }
                        else if (tagName.equalsIgnoreCase("title") ) isTitle = false;
                        else if ( tagName.equalsIgnoreCase("link") ) isLink = false;
                        else if ( tagName.equalsIgnoreCase("description")) isDesc = false;

                        break;
                }
            }




        } catch (XMLStreamException e) {
            e.printStackTrace();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }



        for(RSSitem str : listItems )
            System.out.println(str);


    }
}
