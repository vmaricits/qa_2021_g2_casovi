import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.util.ArrayList;
import java.util.Scanner;

public class CsvMain {

    public static void xmlToCsv (File xml) {

        try {
            Document doc = XMLFasada.open(xml);
            NodeList list = doc.getDocumentElement().getChildNodes();
            String name = "";
            ArrayList<String> data = new ArrayList<>();
            String header = "";
            for(int i = 0 ; i < list.getLength() ; i++ ) {
                if ( list.item(i).getNodeType() == Node.ELEMENT_NODE ) {
                    Element elem = (Element) list.item(i);
                    name = elem.getTagName();
                    String line = "";
                    header = "";
                    NodeList childs = elem.getChildNodes();


                    for( int j = 0 ; j < childs.getLength() ; j++ ) {
                        if( childs.item(j).getNodeType() == Node.ELEMENT_NODE ) {
                            Element childEl = (Element) childs.item(j);
                            line += childEl.getTextContent() + ( j == childs.getLength() - 1 ? "" : "," );
                            header += childEl.getTagName() + ",";
                        }
                    }

                    header = header.substring(0, header.length() - 1);
                    data.add(line);
                }
            }

            FileWriter fw  = new FileWriter(new File(name + ".csv"));
            fw.write(header);
            for(String str : data )
                fw.write(str);
            fw.close();


        } catch (Exception e) {
            e.printStackTrace();
        }

    }


    public static void csvToXml (File csv) {
        try {

            Scanner sc = new Scanner(csv);
            String firstLine = sc.nextLine();
            String[] attributes = firstLine.split(",");
            Document xml = XMLFasada.create();

            xml.appendChild(xml.createElement("root"));

            String name = csv.getName().substring(0, csv.getName().indexOf('.'));

            while(sc.hasNextLine()) {
                String line = sc.nextLine();
                String[] lineData = line.split(",");
                if( lineData.length != attributes.length )
                    continue;

                Element main = xml.createElement(name);
                for(int i = 0 ; i < attributes.length ; i++ ) {
                    Element elem = xml.createElement(attributes[i]);
                    elem.setTextContent(lineData[i]);
                    main.appendChild(elem);
                }

                xml.getDocumentElement().appendChild(main);
            }

            XMLFasada.save(xml, new File(name + ".xml"));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    public static void main(String[] args) throws Exception{

        xmlToCsv(new File("test.xml"));


    }
}
