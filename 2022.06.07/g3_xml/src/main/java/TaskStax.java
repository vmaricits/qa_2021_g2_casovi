import javax.xml.stream.*;
import javax.xml.stream.events.Characters;
import javax.xml.stream.events.EndElement;
import javax.xml.stream.events.StartElement;
import javax.xml.stream.events.XMLEvent;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;

public class TaskStax {

    public static void getTasksForUser(String user) {

        XMLInputFactory factory = XMLInputFactory.newInstance();
        boolean isTask = false, isTitle = false, isDesc = false, isAssignTo = false;
        Task task = null;
        try {
            XMLEventReader eventReader = factory.createXMLEventReader(new FileReader("tasks.xml"));

            while(eventReader.hasNext()) {

                XMLEvent event = eventReader.nextEvent();
                switch ( event.getEventType() ) {
                    case XMLStreamConstants.START_ELEMENT:
                        StartElement openTag = event.asStartElement();
                        String tagName = openTag.getName().getLocalPart();
                        if( tagName.equalsIgnoreCase("task") ) {
                            isTask = true;
                            task = new Task();
                        }
                        else if (tagName.equalsIgnoreCase("title") ) isTitle= true;
                        else if ( tagName.equalsIgnoreCase("desc") ) isDesc = true;
                        else if ( tagName.equalsIgnoreCase("assignto")) isAssignTo = true;

                        break;
                    case XMLStreamConstants.CHARACTERS:
                        Characters characters = event.asCharacters();

                        if( isTitle && task != null ) task.setTitle(characters.getData().trim());
                        else if( isDesc && task != null) task.setDesc(characters.getData().trim());
                        else if( isAssignTo && task != null ) task.setAssignto(characters.getData().trim());

                        break;
                    case XMLStreamConstants.END_ELEMENT:
                        EndElement closedTag = event.asEndElement();
                        tagName = closedTag.getName().getLocalPart();

                        if( tagName.equalsIgnoreCase("task") ) {
                            isTask = false;
                            if( task.getAssignto().equals(user) )
                                System.out.println(task);
                            task = null;
                        }
                        else if (tagName.equalsIgnoreCase("title") ) isTitle= false;
                        else if ( tagName.equalsIgnoreCase("desc") ) isDesc = false;
                        else if ( tagName.equalsIgnoreCase("assignto")) isAssignTo = false;

                        break;
                }
            }




        } catch (XMLStreamException e) {
            e.printStackTrace();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }

    public static void addTask(Task t) throws Exception {
        XMLOutputFactory outputFactory = XMLOutputFactory.newInstance();
        XMLInputFactory inputFactory = XMLInputFactory.newInstance();

        XMLEventReader eventReader = inputFactory.createXMLEventReader(new FileReader("tasks.xml"));
        XMLEventWriter eventWriter = outputFactory.createXMLEventWriter(new FileWriter("tasks.xml"));

        while(eventReader.hasNext()) {
            XMLEvent event = eventReader.nextEvent();
            eventWriter.add(event);
            if( event.getEventType() == XMLStreamConstants.START_ELEMENT ) {
                StartElement startElement = event.asStartElement();
                if( startElement.getName().getLocalPart().equals("root")) {
                    // dodajemo novi element
                    // TODO
                }
            }
        }



    }


    public static void main(String[] args) throws  Exception {
        addTask(new Task());
    }
}
