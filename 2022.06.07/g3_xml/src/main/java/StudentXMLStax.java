import javax.xml.stream.*;
import javax.xml.stream.events.Characters;
import javax.xml.stream.events.EndElement;
import javax.xml.stream.events.StartElement;
import javax.xml.stream.events.XMLEvent;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.net.URL;
import java.util.ArrayList;
import java.util.Comparator;

public class StudentXMLStax {
    public static void main(String[] args) {




        ArrayList<Student> listStudent = new ArrayList<>();
        XMLInputFactory factory = XMLInputFactory.newInstance();
        boolean isStudent = false, isIme = false, isPrezime = false, isIndeks = false, isGrad = false;
        Student student = null;
        try {
            XMLEventReader eventReader = factory.createXMLEventReader(new FileReader("studenti.xml"));

            while(eventReader.hasNext()) {

                XMLEvent event = eventReader.nextEvent();
                switch ( event.getEventType() ) {
                    case XMLStreamConstants.START_ELEMENT:
                        StartElement openTag = event.asStartElement();
                        String tagName = openTag.getName().getLocalPart();
                        if( tagName.equalsIgnoreCase("student") ) {
                            isStudent = true;
                            student = new Student();
                        }
                        else if (tagName.equalsIgnoreCase("ime") ) isIme = true;
                        else if ( tagName.equalsIgnoreCase("prezime") ) isPrezime = true;
                        else if ( tagName.equalsIgnoreCase("indeks")) isIndeks = true;
                        else if (tagName.equalsIgnoreCase("grad") ) isGrad = true;

                        break;
                    case XMLStreamConstants.CHARACTERS:
                        Characters characters = event.asCharacters();

                        if( isIme && student != null ) student.setIme(characters.getData().trim());
                        else if( isPrezime && student != null) student.setPrezime(characters.getData().trim());
                        else if( isGrad && student != null ) student.setGrad(characters.getData().trim());
                        else if (isIndeks && student != null ) student.setIndeks(characters.getData().trim());

                        break;
                    case XMLStreamConstants.END_ELEMENT:
                        EndElement closedTag = event.asEndElement();
                        tagName = closedTag.getName().getLocalPart();

                        if( tagName.equalsIgnoreCase("student") ) {
                            isStudent = false;
                            listStudent.add(student);
                            student = null;
                        }
                        else if (tagName.equalsIgnoreCase("ime") )isIme = false;
                        else if ( tagName.equalsIgnoreCase("prezime") ) isPrezime = false;
                        else if ( tagName.equalsIgnoreCase("indeks"))  isIndeks = false;
                        else if (tagName.equalsIgnoreCase("grad") ) isGrad = false;

                        break;
                }
            }




        } catch (XMLStreamException e) {
            e.printStackTrace();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }



//        listStudent.sort( (s1, s2) -> s1.getIme().compareTo(s2.getIme()) );
        listStudent.sort(new Comparator<Student>() {
            @Override
            public int compare(Student o1, Student o2) {

                int res = o1.getIme().toLowerCase().compareTo(o2.getIme().toLowerCase());
                if ( res == 0 ) return o1.getPrezime().toLowerCase().compareTo(o2.getPrezime().toLowerCase());
                else return res;
            }
        });

        for(Student str : listStudent )
            System.out.println(str);


    }
}
