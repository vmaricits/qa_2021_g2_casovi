public class RSSitem {
    private String desc;
    private String title;
    private String link;

    public RSSitem(String desc, String title, String link) {
        this.desc = desc;
        this.title = title;
        this.link = link;
    }

    public RSSitem() {}

    @Override
    public String toString() {
        return "RSSitem{" +
                "desc='" + desc + '\'' +
                ", title='" + title + '\'' +
                ", link='" + link + '\'' +
                '}';
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }
}
