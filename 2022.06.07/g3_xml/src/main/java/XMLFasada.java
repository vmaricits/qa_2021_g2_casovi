import org.w3c.dom.Document;

import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.File;

public class XMLFasada {

    public static Document open(File fp) throws Exception {
        Document doc =
                DocumentBuilderFactory.newInstance()
                        .newDocumentBuilder().parse(fp);
        return doc;
    }

    public static void save(Document doc, File fp) throws Exception  {
        Transformer transform = TransformerFactory.newInstance().newTransformer();
        DOMSource domStablo = new DOMSource(doc);
        StreamResult out = new StreamResult(fp);
        transform.transform(domStablo, out);
    }

    public static Document create() throws  Exception{
        Document doc = DocumentBuilderFactory.newInstance()
                .newDocumentBuilder().newDocument();
        return doc;
    }

}
