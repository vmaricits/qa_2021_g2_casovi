from concurrent.futures import thread
from selenium.webdriver import Chrome,ChromeOptions
from selenium.webdriver.common.by import By
import time , threading

ulazne_vrednosti = [
    "andjelka","goran"
]

opcije = ChromeOptions()
opcije.add_argument("--headless")

def sajt(ulaz): 
    hrom = Chrome(options=opcije) 
    hrom.get("http://localhost/sajt.html") 
    kontrola = hrom.find_element(By.TAG_NAME,"input") 
    for slovo in ulaz:
        kontrola.send_keys(slovo) 
    kontrola.send_keys(" ") 
    ciljni_element = hrom.find_element(By.ID,"target")
    print(ciljni_element.text.strip(),f"Hello {ulaz}")
    assert ciljni_element.text.strip() == f"Hello {ulaz}" 
    time.sleep(2)
    hrom.quit() 

for i in range(50):
    for ulazna_vrednost in ulazne_vrednosti:
        nit = threading.Thread(None,sajt,args=(ulazna_vrednost,))
        nit.start()