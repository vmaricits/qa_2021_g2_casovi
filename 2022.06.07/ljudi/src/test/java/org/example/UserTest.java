package org.example;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.junit.Assert;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.Response;
import java.util.Iterator;

public class UserTest {

    ArrayNode results;

    boolean checkgender(JsonNode user){
        String pol = user.get("gender").asText();
        return "male".equals(pol) || "female".equals(pol);
    }

    @When("poziv apija user")
    public void poziv() throws JsonProcessingException {
        ObjectMapper om = new ObjectMapper();
        Client klijent = ClientBuilder.newClient();
        WebTarget endpoint = klijent.target("https://randomuser.me/api/?results=100");
        Response res = endpoint.request().get();
        String sadrzaj = res.readEntity(String.class);
        JsonNode json = om.readTree(sadrzaj);
        results = (ArrayNode) json.get("results");
    }
    @Then("pol bude ispravan")
    public void proverapola(){
        Iterator<JsonNode> users = results.iterator();
        while(users.hasNext()){
            Assert.assertTrue(checkgender(users.next()));
        }
    }

}
