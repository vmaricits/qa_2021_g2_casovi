import requests

instrumenti = open("podaci.txt","r").readlines()
domen = "gresnik.com"
for instrument in instrumenti:
    instrument = instrument.strip()
    instrument = instrument.split(",")
     
    odgovor = requests.get(f"http://{domen}/instrumenti.php?id={instrument[0]}").text
    if odgovor != instrument[1]: 
        print(f"Cena nije ispravna za instrument {instrument[0]}, ocekivano je {instrument[1]}, dobijeno je {odgovor}")
