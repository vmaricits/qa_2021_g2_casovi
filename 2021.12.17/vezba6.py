import requests

domeni          = ["jadnik.com","gubitnik.com","gresnik.com","javascript.rs"] 
instrumenti     = ["gitara","tambura","klavir","bubanj","harmonika"]
cene            = [200,300,400,500,600]

for domen in domeni:
    print("TESTIRANI DOMEN:",domen)
    for i,instrument in enumerate(instrumenti):  
        ocekivana_cena = cene[i]
        odgovor     = requests.get(f"http://{domen}/instrumenti.php?id={instrument}").text
        print(instrument,odgovor,ocekivana_cena)
        if ocekivana_cena != int(odgovor):
            print("CENA NIJE ISPRAVNA")