from re import S
import requests

ocekivani_odgovori = {
    "12345.67":"dvanaest hiljada tri stotine četrdesetpet dinara i šezdesetsedam para",
    "1345.67":"jedna hiljada tri stotine četrdesetpet dinara i šezdesetsedam para",
    "150000.00":"stotina pedeset hiljada dinara",
    "521346.78":"pet stotina dvadesetjedna hiljada tri stotine četrdesetšest dinara i sedamdesetosam para",
    "999999.99":"devet stotina devedesetdevet hiljada devet stotina devedesetdevet dinara i devedesetdevet para",
    "1.15":"jedan dinar i petnaest para",
    "55.32":"pedesetpet dinara i tridesetdve pare",
    "998.54":"devet stotina devedesetosam dinara i pedesetčetiri pare",
    "9999.99":"devet hiljada devet stotina devedesetdevet dinara i devedesetdevet para",
    "100.00":"stotina dinara",
    "101.00":"stotina jedan dinar",
    "102.00":"stotina dva dinara",
}

ukupno_slucajeva    = len(ocekivani_odgovori)
neuspenih           = 0

for suma in ocekivani_odgovori: 
    ocekivani_odgovor = ocekivani_odgovori[suma]
    odgovor = requests.get(f"http://gresnik.com?amount={suma}").text 
    if ocekivani_odgovori[suma] != odgovor:
        print("Neispravan odgovor za sumu",suma)
        print("Ocekivano:",ocekivani_odgovor)
        print("Dobijeno:",odgovor)
        neuspenih+=1
    else:
        print("Ispravan odgovor za sumu",suma)

print("#"*15,"Izvestaj","#"*15)
print("Ukupno slucajeva:",ukupno_slucajeva)
print("Ukupno uspesnih:",ukupno_slucajeva-neuspenih)
print("Ukupno neuspesnih:",neuspenih)
