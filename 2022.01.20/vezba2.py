import socket,time

moja_uticnica = socket.socket()
moja_uticnica.bind(("localhost",1500))
moja_uticnica.listen()
print("Listening for connections...")

while True:
    uticnica_klijenta,adresa_klijenta = moja_uticnica.accept()
    print("Client connected",adresa_klijenta) 
    poruka_od_klijenta = uticnica_klijenta.recv(256)
    print("Poruka od klijenta",poruka_od_klijenta)
    poruka = f"Vreme: {time.time()}"
    uticnica_klijenta.send(poruka.encode())
