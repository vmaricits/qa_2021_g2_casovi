import socket,time

moja_uticnica = socket.socket()
moja_uticnica.timeout = 5
moja_uticnica.setsockopt(socket.SOL_SOCKET,socket.SO_REUSEADDR,1)
moja_uticnica.bind(("localhost",1500))
moja_uticnica.listen()
print("Listening for connections...")

while True:
    uticnica_klijenta,adresa_klijenta = moja_uticnica.accept()
    print("Client connected",adresa_klijenta) 
    poruka_od_klijenta = uticnica_klijenta.recv(256)
    print("Poruka od klijenta",poruka_od_klijenta)
    brojevi = poruka_od_klijenta.decode().split(",")
    rezultat = int(brojevi[0])+int(brojevi[1])
    poruka = f"Rezultat je: {rezultat}"
    uticnica_klijenta.send(poruka.encode())