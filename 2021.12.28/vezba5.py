import requests

igre = {
    "dovla market":{
        "url":"http://gubitnik.com",
        "igre":[
            {"naziv":"World of warcraft","sifra":"wow","cena":50},
            {"naziv":"Battlefield 2042","sifra":"bf2042","cena":30},
            {"naziv":"Fifa 2022","sifra":"f2022","cena":40}
        ]
    },
    "ivan soft":{
        "url":"http://gresnik.com",
        "igre":[
            {"naziv":"League of Legends","sifra":"lol","cena":0},
            {"naziv":"Days Gone","sifra":"dg","cena":50},
            {"naziv":"Call of Duty Warzone","sifra":"codwz","cena":0}
        ]
    }
}
greske_cene     = 0
greske_nazivi   = 0
for prodavnica in igre: 
    print(f"Provera prodavnice {prodavnica}")
    prodavnica = igre[prodavnica]
    adresa_prodavnice = prodavnica["url"]
    igre_u_prodavnici = prodavnica["igre"] 
    print("Adresa:",adresa_prodavnice)
    for igra_u_prodavnici in igre_u_prodavnici:
        sifra = igra_u_prodavnici["sifra"]
        naziv = igra_u_prodavnici["naziv"]
        cena  = igra_u_prodavnici["cena"] 
        odgovor = requests.get(f"{adresa_prodavnice}/games.php?igra={sifra}").text 
        odgovor = odgovor.split(",")
        if odgovor[0] != naziv:
            greske_nazivi += 1
        if int(odgovor[1]) != cena:
            greske_cene += 1
        if odgovor[0] == naziv and int(odgovor[1]) == cena:
            print(naziv,"is valid")
        else:
            print("Error for",naziv)

print("Total errors nazivi:" , greske_nazivi)
print("Total errors cene:" , greske_cene)