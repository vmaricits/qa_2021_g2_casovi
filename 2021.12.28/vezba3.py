pacijenti = {
    "ivan":{
            "godine":39,
            "kvarovi":[{"naziv":"karies gore desno cetvorka","lecenje":"plomba i lek"},{"naziv":"upala desni","lecenje":"brufen"}]
        },
    "dovla":{
            "godine":50,
            "kvarovi":[{"naziv":"lom dvojka gore levo","lecenje":"krunica"},{"naziv":"karies sestica desno","lecenje":"vadjenje"},{"naziv":"gangrena","lecenje":"cijanid"}]
        },
    "aleksandar":{
            "godine":39,
            "kvarovi":[]
        }
} 
while True:
    odabrani_pacijent = input("Odaberi pacijenta: ")
    pacijent = pacijenti[odabrani_pacijent]
    print("Broj godina:",pacijent["godine"])
    print("Kvarovi:")
    kvarovi = pacijent["kvarovi"]
    for kvar in kvarovi:
        print("Kvar:",kvar["naziv"],"Lecenje:",kvar["lecenje"])

