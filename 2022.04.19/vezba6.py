import requests
from bs4 import BeautifulSoup

def test_ime():
    imena = ["Senad","Ivan","Dovla","Aleksandra","Andjelka","Dragana","Goran"] 
    forma = requests.get("http://javascript.rs/ime.html").text
    strana = BeautifulSoup(forma,features="html.parser")
    element_form = strana.find("form")
    atribut_action = element_form["action"]
    print(atribut_action)
    for ime in imena:
        dobijeno = requests.post(f"http://javascript.rs/{atribut_action}",data={"ime":ime}).text
        ocekivano = ime[::-1]
        print(ocekivano,dobijeno)
        assert ocekivano == dobijeno