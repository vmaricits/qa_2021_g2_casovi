import requests

def test_brojevi():
    for broj in range(0,101):
        ocekivani_odgovor = broj * broj
        odgovor = requests.post("http://javascript.rs/pow.php",data={"broj":broj}).text 
        odgovor = int(odgovor)
        print(odgovor,ocekivani_odgovor)
        assert odgovor == ocekivani_odgovor
