import requests

def test_age():
    for age in range(0,41):
        ocekivano = "Dobro dosao" if age > 13 else "Nedozvoljen pristup"
        odgovor = requests.post("http://javascript.rs/age.php",data={"age":age}).text
        print(odgovor, ocekivano)
        assert ocekivano == odgovor