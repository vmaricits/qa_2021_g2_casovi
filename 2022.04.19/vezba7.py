from random import betavariate
from re import S
import requests
from bs4 import BeautifulSoup
 
ime = "Senad"
res = requests.post(f"http://javascript.rs/obrada.php",data={"username":ime}).text
ime_obrnuto = ime[::-1]
html = BeautifulSoup(res,features="html.parser")
spanovi = html.find_all("span")
prvi_element = spanovi[0]
assert prvi_element.text == ime
drugi_element = spanovi[1]
assert drugi_element.text == ime_obrnuto