import requests
from bs4 import BeautifulSoup

def test_flow():
    korisnik = "aleksandra"
    prva_strana = requests.get(f"http://javascript.rs/strana1.php?korisnik={korisnik}").text
    prva_strana = BeautifulSoup(prva_strana,features="html.parser")
    span_ime = prva_strana.find("span").text
    hiperlink = prva_strana.find("a")["href"] 
    assert span_ime == korisnik
    druga_strana = requests.get(f"http://javascript.rs/{hiperlink}").text
    druga_strana = BeautifulSoup(druga_strana,features="html.parser")
    h3 = druga_strana.find('h3').text
    assert h3 == f"Hello {korisnik}"
    ime2 = druga_strana.find('p').text.split("(")[1].split(")")[0]
    assert ime2 == korisnik
    hiperlink1 = druga_strana.find("a")["href"] 
    treca_strana = requests.get(f"http://javascript.rs/{hiperlink1}").text
    ime_naopako = korisnik[::-1]
    treca_strana = BeautifulSoup(treca_strana,features='html.parser')
    ime_naopako_server = treca_strana.find("span").text
    ime_zagrade_server = treca_strana.text.split("(")[1].split(")")[0]
    assert ime_zagrade_server == korisnik
    assert ime_naopako == ime_naopako_server 