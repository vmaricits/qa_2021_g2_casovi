import requests

suma = input("Unesite svoj budzet: ")

if not suma:
    print("Morate uneti sumu")
    exit(0)

if not suma.isnumeric():
    print("Neispravan format")
    exit(0)

rezultat = requests.get(f"http://gresnik.com?cena={suma}").json()
print("Dostupne igre: ")
for igra in rezultat:
    if "naziv" not in igra or "cena" not in igra:
        continue
    print(igra["naziv"],igra["cena"],"RSD")