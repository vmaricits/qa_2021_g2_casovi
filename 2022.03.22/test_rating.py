import requests

def setup():
    response = requests.get("http://www.gubitnik.com/rating.php").text 
    global dobijeni_podaci
    dobijeni_podaci = dict([(r.split(",")[0],int(r.split(",")[1])) for r in response.replace("Rating,Total<hr>","").rstrip("<br>").split("<br>")])  
    global ocekivani_podaci
    ocekivani_podaci = {}
    fajl = open("filmovi.txt","r")
    ratinzi = [linija.split(",")[10] for linija in fajl.readlines()]
    for r in ratinzi:
        ocekivani_podaci[r] = 1 if not r in ocekivani_podaci else ocekivani_podaci[r] + 1
    

def test_1():
    global dobijeni_podaci
    global ocekivani_podaci
    for kljuc,vrednost in ocekivani_podaci.items():
        assert kljuc in dobijeni_podaci and dobijeni_podaci[kljuc] == vrednost
def test_2():
    global dobijeni_podaci
    global ocekivani_podaci
    for kljuc,vrednost in dobijeni_podaci.items():
        assert kljuc in ocekivani_podaci and ocekivani_podaci[kljuc] == vrednost
def test_3():
    global dobijeni_podaci
    global ocekivani_podaci
    assert dobijeni_podaci == ocekivani_podaci

setup() 